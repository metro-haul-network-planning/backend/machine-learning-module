# machine-learning-module

The Network Planner architecture is shown in the following figure. It is divided in two blocks. The Front-end block contains the interfaces that allow the exchange of information with the other components of Metro Haul control plane. The Back-end block contains the implementation of the computational modules necessary for the planning tool. The open-source Java-based Net2Plan tool has been chosen as the Network Planner framework for placement, planning and reconfiguration of:
VNFs, IT and network resources.

The machine-learning module incorporates algorithms oriented to predictive periodical or off-line network re-optimization, they make use of machine-learning techniques and are developed in Python. The front and back end modules exchange information by means of native Net2Plan .n2p files. 

<p align="center">
  <img src="planning-tool.png" width="550" title="Planning tool">
</p>

## algorithms
This folder contains the network planning algorithms. 

## network-configuration
This folder contains the programs to simulate the network in order to run the algorithms.

## params.py
It is the Python file with the static information about folders, parameters of the network.

## requirements
It is a text file with the list of softwares required to run the algorithms.