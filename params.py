# Data
NUMBER_OFFLINE_DAYS = 1 # Max=50
NUMBER_ONLINE_DAYS = 1 # Max=12

# Traffic
NUM_GW = 1  # number of gateways
NUM_RING = 8
UP_RATIO = 0.25 # uplink ratio
DOWN_RATIO = 0.75   # downlink ratio      
TRAFFIC_DISTRIBUTION = [1]  # useless
UP_DISTRI = [1]    # the length is number of gateways
DOWN_DISTRI = [1]

# Scheduler 
GRANULARITY = 10240  # Mbps, wavelength capacity
OVERALL_EFF_THRESHOLD =  0.90
INDIV_EFF_THRESHOLD = 0.88 # bandwidth efficiency
AGG_EFF_THRESHOLD = 0.89 # this number must be smaller than the efficiency after scheduler

# Offline ILP
NUM_OF_WAV = 300    # number of wavs in each fiber
NUM_OF_FIBER = 1

# parameters in power consumption model
A = 50
MIU = 240
ALPHA = 32.5
ALPHA_SUM = ALPHA + MIU + A
GAMMA = 85
BETA = 25
FAI = 150


## Select the topology for the simulation

## topology_1_source_data
# h = '/home/seba/git-projects/metro-haul-network-planning/backend/database/topology-1/'

## topology_2_source_data
# h = '/home/seba/git-projects/metro-haul-network-planning/backend/database/topology-2/'

## topology_3_source_data
h = '/home/swan/git-projects/metro-haul-network-planning/backend/database/topology-3/'

generated_data = '/home/swan/git-projects/metro-haul-network-planning/backend/machine-learning-module/network-configuration/generated-data/'
# Path Folders
PATH_FOLDER_SOURCE_DATA = h
PATH_FOLDER_GENERATED_DATA = generated_data
PATH_FOLDER_SCHEDULER_DATA = generated_data+'scheduler/'
PATH_FOLDER_OFFLINE_DEMAND_DATA = generated_data+'offline_demand/'
PATH_FOLDER_OFFLINE_DEMAND_RESULTS = generated_data+'offline_demand_results/'

online_day_test = '1'
WEIGHTS = []
