#!/usr/bin/python
# ----------------------------------------------------------------------
# generate demand list of average data of rings, each item in the list contains:
#                    ['src_w']              source of working path
#                    ['src_b']              destination of backup path
#                    ['dst_w']              source of backup path
#                    ['dst_b']              destination of backup path
#                    ['amount']             average hourly data sequence
#         
# ----------------------------------------------------------------------
# input:
#       initial_average_wav_sequece.csv             demand sequences of average data
#       demand_index_descr.pkl                      description of demand sequences index
# output:
#       offline_hourly_demand_list.pkl              sequence of each demand


from __future__ import division
import sys
import csv
import copy
import random
import math
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pickle

from os.path import expanduser
home = expanduser("~")
sys.path.append(home+'/git-projects/metro-haul-network-planning/backend/machine-learning-module/')
from params import *    

def readcsv(filename):
    """
    read a csv file and save data in a 2 dimension array
    """
    size_row = 0
    size_col = 0
    for row in open(filename):
        size_row = size_row+1
        column = row.split(',')
        size_col = len(column)
                
    data = np.zeros((size_row, size_col))
    row_num = 0
    for row in open(filename):
        column = row.split(',')
        for i in range(0, size_col):
            if column[i] == '':
                column[i] = 0
            column[i] = float(column[i])      
        data[row_num] = column
        row_num = row_num+1

    return data, size_row, size_col


def writecsv(data, outputname):
    """
    write data into a csv file
    """
    csvfile = file(outputname, 'wb')
    writer = csv.writer(csvfile)
    writer.writerows(data)
    csvfile.close()


def hourly_demand_gen(traffic_origin, index_descri, NUM_RING, NUM_GW):

    traffic = copy.deepcopy(traffic_origin)
    offline_hourly_demand_list = []
    for i in range(1, NUM_RING + 1):
        for j in range(NUM_GW):
            for index in index_descri.keys():
                # uplink
                if index_descri[index][0] == "ring_w_" + str(i) and index_descri[index][1] == "gw" + str(j):
                    offline_hourly_demand_list.append({})
                    offline_hourly_demand_list[-1]['src_w'] = str(float(i)) + 'w'
                    offline_hourly_demand_list[-1]['src_b'] = str(float(i)) + 'b'
                    offline_hourly_demand_list[-1]['dst_w'] = 'gw' + str(int(j + 1))
                    offline_hourly_demand_list[-1]['dst_b'] = 'gw' + str(int(j + 1))
                    offline_hourly_demand_list[-1]['amount'] = traffic[index + 1, 1:]
                # downlink
                if index_descri[index][0] == "gw" + str(j) and index_descri[index][1] == "ring_w_" + str(i):
                    offline_hourly_demand_list.append({})
                    offline_hourly_demand_list[-1]['src_w'] = 'gw' + str(int(j + 1)) 
                    offline_hourly_demand_list[-1]['src_b'] = 'gw' + str(int(j + 1)) 
                    offline_hourly_demand_list[-1]['dst_w'] = str(float(i)) + 'w'
                    offline_hourly_demand_list[-1]['dst_b'] = str(float(i)) + 'b'
                    offline_hourly_demand_list[-1]['amount'] = traffic[index + 1, 1:]

    return offline_hourly_demand_list



def main(PATH_FOLDER_SCHEDULER, PATH_FOLDER_OFFLINE_DEMANDS):

    for day in range(1, NUMBER_OFFLINE_DAYS+1):
    	name=PATH_FOLDER_SCHEDULER_DATA+'initial_average_wav_sequence_'+str(day)+'.csv'
    	ring_traffic, size_row, size_col = readcsv(name)
    	name=PATH_FOLDER_OFFLINE_DEMAND_DATA+'demand_index_descr_'+str(day)+'.pkl'
    	index_descri = pickle.load(open(name, 'rb'))

    	offline_hourly_demand_list = hourly_demand_gen(ring_traffic, index_descri, NUM_RING, NUM_GW)
            #for item in offline_hourly_demand_list:
    		#print item
    	#print len(offline_hourly_demand_list)

    	name=PATH_FOLDER_OFFLINE_DEMAND_DATA+'offline_hourly_demand_list_'+str(day)+'.pkl'
    	pickle.dump(offline_hourly_demand_list, open(name, 'wb'))


if __name__ == '__main__':

    main(PATH_FOLDER_SCHEDULER_DATA, PATH_FOLDER_OFFLINE_DEMAND_DATA)
