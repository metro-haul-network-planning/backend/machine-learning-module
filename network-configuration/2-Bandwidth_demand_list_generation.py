#!/usr/bin/python
# ----------------------------------------------------------------------
# generate bandwidth demand sequence for each src&dst pair by using the demand 
# information of each aggregation ring
# 
# ----------------------------------------------------------------------
# input: 
#       offline_ring_traffic_demands_milan.csv          offline bandwidth demand data of aggregaton rings
# The output files will be :
#       offline_demand_data.csv                         sequences of demands
#       demand_index_descr.pkl                          demand index description file for offline_demand_data.csv
# 

import os
import sys
import csv
import time
import copy
import random
import math
import pickle
import numpy as np
import scipy.io as sio

from os.path import expanduser
home = expanduser("~")
sys.path.append(home+'/git-projects/metro-haul-network-planning/backend/machine-learning-module/')
from params import *


def readcsv(filename):
    """
    read a csv file and save data in a 2 dimension array
    """
    size_row = 0
    size_col = 0
    for row in open(filename):
        size_row = size_row+1
        column = row.split(',')
        size_col = len(column)
                
    data = np.zeros((size_row, size_col))
    row_num = 0
    for row in open(filename):
        column = row.split(',')
        for i in range(0, size_col):
            if column[i] == '' or column[i] == '\n':
                column[i] = 0
            column[i] = float(column[i])      
        data[row_num] = column
        row_num = row_num + 1

    return data, size_row, size_col


def writecsv(data, outputname):
    """
    write data into a csv file
    """
    csvfile = file(outputname, 'wb')
    writer = csv.writer(csvfile)
    writer.writerows(data)
    csvfile.close()


def main():

    # NUM_GW = 1  # number of gateways
    # UP_RATIO = 0.25 # uplink ratio
    # DOWN_RATIO = 0.75   # downlink ratio      
    # TRAFFIC_DISTRIBUTION = [1]  # useless
    # NUMBER_OFFLINE_DAYS = 50
    # NUMBER_ONLINE_DAYS = 12

    if not os.path.exists(PATH_FOLDER_OFFLINE_DEMAND_DATA):
        os.makedirs(PATH_FOLDER_OFFLINE_DEMAND_DATA)

    # input_file = PATH_FOLDER_GENERATED_DATA+'offline_ring_traffic_demands_milan.csv' 
    input_file = PATH_FOLDER_SOURCE_DATA+'offline_ring_traffic_demands.csv'
    data, size_row, size_col = readcsv(input_file)

    d=1	#I start from the day 1
    for cont in range(0, (size_col-1)/24): # I do for 50 days
		demand_index_descr = {}   # describ demand index with src&dst info
		out = []                  # demand sequences of each src&dst pair
		out.append(data[0, 0:25])
		demand_index = 0
		for i in range(1, size_row): # Spanish network has 5 nodes
			for k in range(NUM_GW):
				# for uplink
				demand_sequence = []
				demand_sequence.append(demand_index)
				if cont==1:
					f=1
				else:
					f=(cont-1)*24+1
				
				for j in range(f, (cont)*24+1):# (1, size_col)
					demand_sequence.append(data[i][j] * UP_RATIO * TRAFFIC_DISTRIBUTION[k])		        	
					
				out.append(demand_sequence)
				demand_index_descr[demand_index] = ["ring_w_" + str(i-1), "gw" + str(k)]
				demand_index += 1
					
				demand_sequence = []
				demand_sequence.append(demand_index)
				if cont==1:
					e=1
				else:
					e=(cont-1)*24+1
						
				for j in range(e, (cont)*24+1):
					demand_sequence.append(data[i][j] * UP_RATIO * TRAFFIC_DISTRIBUTION[k])
				out.append(demand_sequence)
				demand_index_descr[demand_index] = ["ring_b_" + str(i-1), "gw" + str(k)]
				demand_index += 1

				# for downlink
				demand_sequence = []
				demand_sequence.append(demand_index)
				if cont==1:
					r=1
				else:
					r=(cont-1)*24+1
						
				for j in range(r, (cont)*24+1):
					demand_sequence.append(data[i][j] * DOWN_RATIO * TRAFFIC_DISTRIBUTION[k])
				out.append(demand_sequence)
				demand_index_descr[demand_index] = ["gw" + str(k), "ring_w_" + str(i-1)]
				demand_index += 1

				demand_sequence = []
				demand_sequence.append(demand_index)
				if cont==1:
					t=1
				else:
					t=(cont-1)*24+1
						
				for j in range(t ,(cont)*24+1):
					demand_sequence.append(data[i][j] * DOWN_RATIO * TRAFFIC_DISTRIBUTION[k])
				out.append(demand_sequence)
				demand_index_descr[demand_index] = ["gw" + str(k), "ring_b_" + str(i-1)]
				demand_index += 1

		filename=PATH_FOLDER_OFFLINE_DEMAND_DATA+'offline_demand_data_'+str(d)+'.csv'
		out = np.array(out)
		writecsv(out,filename)
		name=PATH_FOLDER_OFFLINE_DEMAND_DATA+'demand_index_descr_'+str(d)+'.pkl'
		# print demand_index_descr
		pickle.dump(demand_index_descr, open(name, 'wb'))
		d=d+1
		

if __name__ == '__main__':

    main()
