#!/usr/bin/python
# ----------------------------------------------------------------------
# pick peak value of each demand from real time data for dimensioning, and generate 
# real time sequence of each demand.
# 
# ----------------------------------------------------------------------
# input:
#       online_ring_traffic_demands_milan.csv
#       planar_graph
#       position_file.pkl
#       color_list.pkl 
# output:
#       peak_demand_list.pkl            peak value of each demand
#       online_hourly_demand_list.pkl          sequence of each demand

from __future__ import division
import csv
import sys
import copy
import random
import math
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pickle

from os.path import expanduser
home = expanduser("~")
sys.path.append(home+'/git-projects/metro-haul-network-planning/backend/machine-learning-module/')
from params import *

def readcsv(filename):
    """
    read a csv file and save data in a 2 dimension array
    """
    size_row = 0
    size_col = 0
    for row in open(filename):
        size_row = size_row+1
        column = row.split(',')
        size_col = len(column)
                
    data = np.zeros((size_row, size_col))
    row_num = 0
    for row in open(filename):
        column = row.split(',')
        for i in range(0, size_col):
            if column[i] == '':
                column[i] = 0
            column[i] = float(column[i])      
        data[row_num] = column
        row_num = row_num+1

    return data, size_row, size_col


def writecsv(data, outputname):
    """
    write data into a csv file
    """
    csvfile = file(outputname, 'wb')
    writer = csv.writer(csvfile)
    writer.writerows(data)
    csvfile.close()


def to_directed(graph):
    '''
    convert undireced graph to directed graph
    '''
    digraph = graph.to_directed()
    for (i,j) in digraph.edges():
	digraph.add_edge(j,i)
    return digraph

def peak_demand_gen(traffic_origin, uprate, downrate, updistri, downdistri, granularity):
    '''
    pick peak value from each demand
    '''
    traffic = copy.deepcopy(traffic_origin)
    traffic_dict = {}
    (row, col) = np.shape(traffic)
    for i in range(1,row):
    	traffic_dict[traffic[i][0]] = traffic[i][1:] # I use the first month for the network dimensioning

    peak_demand = {}
    for key in traffic_dict.keys():
    	traffic_dict[key].sort()
    	peak_demand[key] = traffic_dict[key][-1]

    peak_demand_list = []
    k = len(updistri)  # number of gateways
    for key in peak_demand.keys():
    	for i in range(k):
    		peak_demand_list.append({})
    		peak_demand_list[-1]['src_w'] = str(key) + 'w'
    		peak_demand_list[-1]['src_b'] = str(key) + 'b'
    		peak_demand_list[-1]['dst_w'] = 'gw' + str(int(i+1))
    		peak_demand_list[-1]['dst_b'] = 'gw' + str(int(i+1))
    		peak_demand_list[-1]['amount'] = math.ceil(peak_demand[key] 
    			                             * uprate * updistri[i]/granularity)

    	for i in range(k):
    		peak_demand_list.append({})
    		peak_demand_list[-1]['src_w'] = 'gw' + str(int(i+1)) 
    		peak_demand_list[-1]['src_b'] = 'gw' + str(int(i+1)) 
    		peak_demand_list[-1]['dst_w'] = str(key) + 'w'
    		peak_demand_list[-1]['dst_b'] = str(key) + 'b'
    		peak_demand_list[-1]['amount'] = math.ceil(peak_demand[key] 
    			                             * downrate * downdistri[i]/granularity)

    return peak_demand_list

def hourly_demand_gen(traffic_origin, uprate, downrate, updistri, downdistri, granularity):
    '''
    for each demand, generate a hourly sequence, and each demand includes src&dst info
    '''
    traffic = copy.deepcopy(traffic_origin)
    traffic_dict = {}
    (row, col) = np.shape(traffic)
    for i in range(1,row):
        traffic_dict[traffic[i][0]] = traffic[i][1:]

    # split traffic of node into demands 
    online_hourly_demand_list = []
    k = len(updistri)  # number of gateways

    for key in traffic_dict.keys():
        if key < row-1: #52-> because one node has been considered gateway

            for i in range(k):
                # uplinks
                online_hourly_demand_list.append({})
                online_hourly_demand_list[-1]['src_w'] = str(key) + 'w'
                online_hourly_demand_list[-1]['src_b'] = str(key) + 'b'
                online_hourly_demand_list[-1]['dst_w'] = 'gw' + str(int(i+1))
                online_hourly_demand_list[-1]['dst_b'] = 'gw' + str(int(i+1))
                online_hourly_demand_list[-1]['amount'] = []
                
                # len(traffic_dict[key]) = 288 -> total hours
                # total_days = len(traffic_dict[key])/24 = 12
                # just the first day -> 288-264 = 24
                
                for j in range(len(traffic_dict[key])-264):
                    online_hourly_demand_list[-1]['amount'].append(math.ceil(traffic_dict[key][j] 
                                                       * uprate * updistri[i]/granularity))
            for i in range(k):
                # downlinks
                online_hourly_demand_list.append({})
                online_hourly_demand_list[-1]['src_w'] = 'gw' + str(int(i+1)) 
                online_hourly_demand_list[-1]['src_b'] = 'gw' + str(int(i+1)) 
                online_hourly_demand_list[-1]['dst_w'] = str(key) + 'w'
                online_hourly_demand_list[-1]['dst_b'] = str(key) + 'b'
                online_hourly_demand_list[-1]['amount'] = []
                for j in range(len(traffic_dict[key])-264):
                    online_hourly_demand_list[-1]['amount'].append(math.ceil(traffic_dict[key][j] 
                                                 * downrate * downdistri[i]/granularity))
                                             
                                             
    # convert hourly amount value to houly relative amount value
    for item in online_hourly_demand_list:
        for i in range(len(item['amount']) - 1, 0, -1):
            item['amount'][i] = item['amount'][i] - item['amount'][i - 1]
        # print item['amount']

    return online_hourly_demand_list

def main():

    G = nx.Graph()
    G = nx.read_edgelist(PATH_FOLDER_GENERATED_DATA+'planar_graph', nodetype = str, create_using = nx.Graph())
    pos = pickle.load(open(PATH_FOLDER_GENERATED_DATA+'position_file.pkl', 'rb'))
    color_list = pickle.load(open(PATH_FOLDER_GENERATED_DATA+'color_list.pkl','rb'))
    G = to_directed(G)
    nx.draw_networkx(G, pos, with_labels = False, node_size = 50, node_color = color_list)

    # UP_RATIO = 0.25     # uplink ratio
    # DOWN_RATIO = 0.75   # downlink ratio
    # UP_DISTRI = [1]    # the length is number of gateways
    # DOWN_DISTRI = [1]
    # GRANULARITY = 10240 #MBps

    # row = 73 # topology milan small
    
    ring_traffic, size_row, size_col = readcsv(PATH_FOLDER_SOURCE_DATA+'online_ring_traffic_demands.csv') # Real traffic
    
    ring_traffic_offline, size_row_offline, size_col_offline = readcsv(PATH_FOLDER_SOURCE_DATA+'offline_ring_traffic_demands.csv')

    peak_demand_list = peak_demand_gen(ring_traffic_offline, UP_RATIO, DOWN_RATIO, 
    	                               UP_DISTRI, DOWN_DISTRI, GRANULARITY)
    online_hourly_demand_list = hourly_demand_gen(ring_traffic, UP_RATIO, DOWN_RATIO, 
                                       UP_DISTRI, DOWN_DISTRI, GRANULARITY)

    pickle.dump(peak_demand_list, open(PATH_FOLDER_GENERATED_DATA+'peak_demand_list.pkl', 'wb'))
    pickle.dump(online_hourly_demand_list, open(PATH_FOLDER_GENERATED_DATA+'online_hourly_demand_list.pkl', 'wb'))


if __name__ == '__main__':

	main()
