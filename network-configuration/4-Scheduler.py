#!/usr/bin/python
# ----------------------------------------------------------------------
# read traffic demand csv file and build individual scheduler and global scheduler, 
# perfom reconfiguration points reduction inside scheduler using greedy algorithm
#
#
# then reconfiguration points aggreagtion procedure, firstly use greedy
# algorithm to find minimum R time points to satify efficiency requirement,
# then use Simulated Annealing algorithm to find a better solution based on 
# greedy result
#         
# ----------------------------------------------------------------------
# input:
#       offline_demand_milan.csv           offline bandwidth demand sequnces for demands(src&dst pairs) 

#
# output:
#       initial_average_wav_sequence.csv        initial wavelength numbers of input data
#       change_greedy.csv                       wavelength changes in each reconfiguration time after greedy algorithm
#       states_greedy.csv                       wavelength numbers in each reconfiguration time after greedy algorithm
#       change_SA.csv                           wavelength changes in each reconfiguration time after SA algorithm
#       states_SA.csv                           wavelength numbers in each reconfiguration time after SA algorithm

from __future__ import division
import os
import sys
import csv
import time
import copy
import random
import math
import pickle
import numpy as np
import scipy.io as sio

import os
from os.path import expanduser
home = expanduser("~")
sys.path.append(home+'/git-projects/metro-haul-network-planning/backend/machine-learning-module/')
from params import *


class ReconPoint(object):
    def __init__(self):
        self.label = None
        self.time = None
        self.change = None
        self.current = None

class cluster(object):
    def __init__(self):
        self.ID = None
        self.traffic_array = []
        self.stepwise = []
        self.recon_points = []
        self.num_on = None
        self.num_off = None
        self.full_states = [None for i in range(24)]


def readcsv(filename):
    size_row = 0
    size_col = 0
    for row in open(filename):
        size_row = size_row+1
        column = row.split(',')
        size_col = len(column)
                
    data = np.zeros((size_row, size_col))
    row_num = 0
    for row in open(filename):
        column = row.split(',')
        for i in range(0,size_col):
            if column[i] == '':
                column[i] = 0
            column[i] = float(column[i])      
        data[row_num] = column
        row_num = row_num+1

    return data, size_row, size_col


def writecsv(data, outputname):
    csvfile = file(outputname, 'wb')
    writer = csv.writer(csvfile)
    writer.writerows(data)
    csvfile.close()


def Stepwise(sequence, GRANULARITY):
    """
    calculated initial allocated bandwith curve (stepwise curve) based 
    on allocation GRANULARITY and mark a label on each reconfiguration point
    """
    stepseq = []
    num_on = 0
    num_off = 0
    
    for i in range(len(sequence)):
        if sequence[i]%GRANULARITY != 0:
            stepseq.append((sequence[i]//GRANULARITY+1) * GRANULARITY)
        else:
            stepseq.append(sequence[i])

    # assume first point is a switch-on point
    recon_points = []

    # complete the information about each reconfiguration point 
    for i in range(len(sequence)):
        if (stepseq[i]-stepseq[i-1])>0:
            recon_points.append(ReconPoint())
            recon_points[-1].label = 'switch-on'
            recon_points[-1].time = i
            recon_points[-1].change = stepseq[i]-stepseq[i-1]
            recon_points[-1].current = stepseq[i]
            num_on+=1

        elif (stepseq[i]-stepseq[i-1])<0:
            recon_points.append(ReconPoint())
            recon_points[-1].label = 'switch-off'
            recon_points[-1].time = i
            recon_points[-1].change = stepseq[i]-stepseq[i-1]
            recon_points[-1].current = stepseq[i]
            num_off+=1
            
    return stepseq, recon_points, num_on, num_off


def individual_shceduler(cluster_node, GRANULARITY, num_threshold, eff_threshold):
    """
    design of scheduler for each aggregation node, here the cluster means aggregation node
    """
    cluster_node.stepwise,cluster_node.recon_points,\
                cluster_node.num_on, cluster_node.num_off=\
                Stepwise(cluster_node.traffic_array, GRANULARITY)

    # calculate bandwidth efficiency
    demand = sum(cluster_node.traffic_array)
    bandwidth = sum(cluster_node.stepwise)
    efficiency = demand/bandwidth

    point = cluster_node.recon_points
    # the loop is a greedy algorithm for reconfiguration points reduction in each aggregation node   
    while len(cluster_node.recon_points) > num_threshold and efficiency > eff_threshold:
        position = 0
        step_final = 0
        condition_min = 0
        minimum = float('Inf')

        for i in range(len(point)):
            loss = 0
            condition = 0
            if point[i].label == 'switch-on':
                if i != 0:
                    condition = 1
                    if point[i].change+point[i-1].change == 0:
                        loss = (point[i].time-point[i-1].time)*point[i].change/2
                    else:
                        loss = (point[i].time-point[i-1].time)*point[i].change

                else:
                    condition = 2
                    if point[i].change+point[i-1].change == 0:
                        loss = (point[i].time+24-point[i-1].time)*point[i].change/2
                    else:
                        loss = (point[i].time+24-point[i-1].time)*point[i].change

            else:
                if i != len(point)-1:
                    condition = 3
                    if point[i].change+point[i+1].change == 0:
                        loss = (point[i+1].time-point[i].time)*(-point[i].change)/2
                    else:
                        loss = (point[i+1].time-point[i].time)*(-point[i].change)
                else:
                    condition = 4
                    if point[i].change+point[0].change == 0:
                        loss = (point[0].time+24-point[i].time)*(-point[i].change)/2
                    else:
                        loss = (point[0].time+24-point[i].time)*(-point[i].change)

            if loss < minimum:
                minimum = loss
                position = i
                condition_min = condition

        if condition_min == 1 or condition_min == 2:
            if point[position].change+point[position-1].change != 0:
                point[position-1].current = point[position].current
                point[position-1].change += point[position].change
                if point[position-1].change > 0:
                    point[position-1].label = 'switch-on'
                else:
                    point[position-1].label = 'switch-off'
                del point[position]
            else:
                minimum = minimum*2
                del point[position]
                del point[position-1]

        elif condition_min == 3:
            if point[position].change+point[position+1].change != 0:
                point[position+1].change += point[position].change
                if point[position+1].change > 0:
                    point[position+1].label = 'switch-on'
                else:
                    point[position+1].label = 'switch-off'
                del point[position]
            else:
                minimum = minimum*2
                del point[position+1]
                del point[position]

        elif condition_min == 4:
            if point[position].change+point[0].change != 0:
                point[0].change += point[position].change
                if point[0].change > 0:
                    point[0].label = 'switch-on'
                else:
                    point[0].label = 'switch-off'
                del point[position]
            else:
                minimum = minimum*2
                del point[0]
                del point[position]
        bandwidth += minimum
        efficiency = demand/bandwidth

    list_recon_time = []
    list_recon_state = [None for i in range(24)]
    for item in point:
        list_recon_time.append(item.time)
        list_recon_state[item.time] = item.current
    if len(point) != 0:
        for i in range(point[0].time, 24+point[0].time):
            if list_recon_state[i%24] != None:
                cluster_node.full_states[i%24] = list_recon_state[i%24]
            else:
                cluster_node.full_states[i%24] = cluster_node.full_states[(i-1)%24]
    else:
        cluster_node.full_states = cluster_node.stepwise

def global_scheduler(filename, GRANULARITY, OVERALL_EFF_THRESHOLD, INDIV_EFF_THRESHOLD):
    """
    design of global scheduler, decide number of reconfiguration points and
    efficiency threshold for individual scheduler 
    """
    traffic, size_r, size_c = readcsv(filename)
    
    num_threshold = 0
    eff_schedul = 0
    while eff_schedul < OVERALL_EFF_THRESHOLD:
        cluster_array = []
        num_threshold += 1
        for i in range(1, size_r):
            # complete cluster elements of each cluster
            cluster_array.append(cluster())
            cluster_array[-1].ID = traffic[i][0]
            cluster_array[-1].traffic_array = traffic[i][1:]

            individual_shceduler(cluster_array[-1], GRANULARITY, num_threshold, INDIV_EFF_THRESHOLD)
                                   
        total_demand = 0
        allocation_stepwise = 0
        for item in cluster_array:
            total_demand += sum(item.traffic_array)
            allocation_stepwise += sum(item.stepwise)
            #print item.stepwise
        total_efficiency = total_demand/allocation_stepwise
       

        if OVERALL_EFF_THRESHOLD > total_efficiency:
            print "Error: the efficiency requirement after scheduling is higher than that before scheduling"
            print "please set an efficiency threshold after after scheduling  lower than %f" %(total_efficiency)
            break

        alloc_schedul = 0
        for i in range(len(cluster_array)):
            alloc_schedul += sum(cluster_array[i].full_states)
        eff_schedul = total_demand/alloc_schedul
    print "--------------------scheduler-----------------------"
    print "total traffic demand is %f TB" %(total_demand/8*3600/(1204**2))
    print "total efficiency between total demand and stepwise allocation is %f" %(total_efficiency)
    print "in order to satisfy the efficiency requirement, number of reconfiguration points in each aggregation node should be %d" \
           %(num_threshold)
    #print "total allocation after scheduling is %f TB" %(alloc_schedul/8*3600/(1204**2))
    print "total efficiency after scheduling is %f" %(eff_schedul)
    print "----------------------------------------------------"

    # make a table for reconfiguration points' changes, the column is time and row is cluster ID
    recon_table = np.zeros((size_r,size_c))
    # initialize the first row (time)
    for i in range(1,size_c):
        recon_table[0][i] = i-1
    for i in range(1,size_r):
        recon_table[i][0] = cluster_array[i-1].ID
        for item in cluster_array[i-1].recon_points:
            recon_table[i][item.time+1] = item.change 
    # writecsv(recon_table, 'change_after_scheduler.csv')

    # make a table for reconfiguration points' current states, the column is time and row is cluster ID
    states_table = np.zeros((size_r, size_c))
    # initialize the first row (time)
    for i in range(1, size_c):
        states_table[0][i] = i-1        
    for i in range(1, size_r):
        states_table[i][0] = cluster_array[i-1].ID
        for item in cluster_array[i-1].recon_points:
            states_table[i][item.time+1] = item.current 
    #writecsv(states_table, 'states_after_scheduler.csv')

    # make a full states table, the column is time and row is cluster ID
    full_table = np.zeros((size_r, size_c))
    for i in range(1, size_c):
        full_table[0][i] = i-1        
    for i in range(1, size_r):
        full_table[i][0] = cluster_array[i-1].ID
        full_table[i,1:] = cluster_array[i-1].full_states 
    #writecsv(full_table, 'full_states_scheduler.csv')

    return recon_table, states_table, full_table


def ComputeLoss(previous, current, following, point_list, state_table):
    """
    calculate bandwidth loss in each aggregation step
    """
    size_row, size_col = np.shape(state_table)
    # just copy, do not pass reference
    state_copy = copy.deepcopy(state_table)
    state_copy1 = state_copy[:, 1:]
    list_copy = copy.deepcopy(point_list)

    band_loss = 0
    for row in list_copy[current]:
        if state_copy1[row][current]-state_copy1[row][current-1] < 0:
            if following >= current:
                state_copy1[row][current:following] = [state_copy1[row][current-1]
                                                       for i in range(following-current)]
                if state_copy1[row][following] == state_copy1[row][following-1]:
                    if row in list_copy[following]:
                        list_copy[following].remove(row)
                else:
                    if row not in list_copy[following]:
                        list_copy[following].append(row)
            else:
                state_copy1[row][current:] = [state_copy1[row][current-1]
                                                       for i in range(24-current)]
                state_copy1[row][0:following] = [state_copy1[row][current-1]
                                                       for i in range(following)]
                if state_copy1[row][following] == state_copy1[row][following-1]:
                    if row in list_copy[following]:
                        list_copy[following].remove(row)
                else:
                    if row not in list_copy[following]:
                        list_copy[following].append(row)
   

                    
        elif state_copy1[row][current]-state_copy1[row][current-1] > 0:
            if previous <= current:
                state_copy1[row][previous:current] = [state_copy1[row][current]
                                                       for i in range(current-previous)]
                if state_copy1[row][previous-1] == state_copy1[row][previous]:
                    if row in list_copy[previous]:
                        list_copy[previous].remove(row)
                else:
                    if row not in list_copy[previous]:
                        list_copy[previous].append(row)
            else:
                state_copy1[row][previous:] = [state_copy1[row][current] 
                                                        for i in range(24-previous)]
                state_copy1[row][0:current] = [state_copy1[row][current] 
                                                        for i in range(current)]
                if state_copy1[row][previous-1] == state_copy1[row][previous]:
                    if row in list_copy[previous]:
                        list_copy[previous].remove(row)
                else:
                    if row not in list_copy[previous]:
                        list_copy[previous].append(row)

    del list_copy[current]

    # calculate bandwidth loss
    band_loss = state_copy-state_table
    band_loss = sum(sum(band_loss))
    return band_loss, state_copy, list_copy


def AggregationGreedy(num_time, state_table):
    """
    aggregate time points of reconfiguration points using greedy algorithm
    """
    time_array = [i for i in range(24)] 
    table_greedy = copy.deepcopy(state_table)
    total_loss = 0

    state_table_copy = copy.deepcopy(state_table[:, 1:])
    size_row, size_col = np.shape(state_table_copy)
    recon_list = {}
    for i in range(24): 
        recon_list[i] = []
    for i in range(1, size_row):
        for j in range(size_col):
            if state_table_copy[i][j] != state_table_copy[i][j-1]:
                recon_list[j].append(i)

    
    while num_time < len(time_array):
        minimum = float('Inf')
        minimum_table = None
        position = None
        list_min = None
        for i in range(len(time_array)):
            loss, copy_greedy, list_copy = ComputeLoss(time_array[i-1], time_array[i],
                                                           time_array[(i+1)%len(time_array)], recon_list, table_greedy)
            if loss < minimum:
                minimum = loss
                minimum_table = copy_greedy
                position = i
                list_min = list_copy

        table_greedy = minimum_table
        total_loss += minimum
        recon_list = list_min
        del time_array[position]

    return time_array, total_loss, table_greedy

def neighbour(time_array_origin):
    """
    find a neighbor time array_neighbour
    """
    time_array = copy.deepcopy(time_array_origin)
    length = len(time_array)
    time_array_copy = random.sample(time_array, length-1)
    while len(time_array_copy) != length:
        rand=random.randint(0, 23)
        if rand not in time_array_copy:
            time_array_copy.append(rand)
    time_array_copy = sorted(time_array_copy)
    return time_array_copy


def InitialArray(num_time):
    """
    randomly generate a time array with length num_time
    """
    list_full = [i for i in range (24)]
    time_array = random.sample(list_full, num_time)
    time_array = sorted(time_array)
    return time_array


def TimeAggregation(time_array, state_table):
    """
    aggregate reconfiguration points into time points given by time_array
    """
    size_row, size_col = np.shape(state_table)    
    # just copy, do not pass reference
    state_copy = copy.deepcopy(state_table)

    
    for i in range(len(time_array)):
        if i != len(time_array)-1:
            state_copy[1:,time_array[i]+1:time_array[i+1]+1] = \
                    [[max(state_copy[k,time_array[i]+1:time_array[i+1]+1])] for k in range(1,size_row)]
        else:
            if time_array[0] != 0:
                state_copy[1:,time_array[i]+1:] = \
                        [[max(max(state_copy[k,time_array[i]+1:]),
                        max(state_copy[k, 1:time_array[0]+1]))] for k in range(1,size_row)]
                state_copy[1:,1:time_array[0]+1] = \
                        [[max(max(state_copy[k,time_array[i]+1:]),
                        max(state_copy[k, 1:time_array[0]+1]))] for k in range(1,size_row)]
            else:
                state_copy[1:,time_array[i]+1:] = \
                        [[max(state_copy[k,time_array[i]+1:])] for k in range(1,size_row)]

    band_loss = state_copy-state_table
    band_loss = sum(sum(band_loss))    
    return band_loss, state_copy


def SimulatedAnnealing(num_time, array_initial, states):
    """
    Simulated Annealing procedure to find a better solution
    """
    # initial solution
    time_array = array_initial
    loss_initial, table_initial = TimeAggregation(time_array, states)
    # SA parameters
    mu = 0.3
    phi = 0.2
    T0 = -mu/(math.log(phi))*loss_initial
    N_k = 100*num_time
    N_T = 100
    alpha = 0.9
    constant = 200*num_time


    print "-----------------------SA---------------------------"
    start_time = float(time.time())
    
    count_T = 0
    loss_current = loss_initial
    table_current = table_initial
    loss_min = loss_initial
    array_min = copy.deepcopy(time_array)
    count_i = 0
    table_min = copy.deepcopy(table_initial)

    while count_T < N_T and count_i < constant:
        count_k = 0            
        while count_k < N_k and count_i < constant:
            array_neighbour = neighbour(time_array)
            loss_neighbour, table_neighbour = TimeAggregation(array_neighbour, states)
            if loss_neighbour <= loss_current:
                time_array = array_neighbour
                loss_current = loss_neighbour
                table_current = table_neighbour
            elif math.exp((loss_min-loss_neighbour)/T0) > random.random():
                time_array = array_neighbour
                loss_current = loss_neighbour
                table_current = table_neighbour
                
            if loss_current < loss_min :
                loss_min = loss_current
                array_min = copy.deepcopy(time_array)
                table_min = table_current
                count_i = 0
            else:
                count_i += 1
        
            count_k += 1
        T0 = alpha*T0
        count_T += 1

    end_time = float(time.time())
    print "time consumption of SA is %f ms" %((end_time - start_time)*10**3 )
    print "-----------------------------------------------------"

    return array_min, loss_min, table_min

def write_change(table, filename, GRANULARITY):
    """
    write change values of reconfiguration points into a csv file
    """
    size_row, size_col = np.shape(table)
    table_out = np.zeros((np.shape(table)))
    table_out[0,:] = copy.deepcopy(table[0,:])
    table_out[:,0] = copy.deepcopy(table[:,0])
    for i in range(1, size_row):
        for j in range (1, size_col):
            if j == 1:
                if table[i][j]-table[i][-1] != 0:
                    change = table[i][j]-table[i][-1]
                    table_out[i][j] = change / GRANULARITY
            else:
                if table[i][j]-table[i][j-1] != 0:
                    change = table[i][j]-table[i][j-1] 
                    table_out[i][j] = change / GRANULARITY
    writecsv(table_out, filename)

def write_states(table, filename, GRANULARITY, time_array):
    """
    write state values of reconfiguration points into a csv file
    """
    size_row, size_col = np.shape(table)
    table_out = np.zeros((np.shape(table)))
    table_out[0,:] = copy.deepcopy(table[0,:])
    table_out[:,0] = copy.deepcopy(table[:,0])
    for i in range(1, size_row):
        for j in range (1, size_col):
            if j ==1:
                if table[i][j]-table[i][-1] != 0 or (j -1) in time_array:
                    table_out[i][j] = table[i][j] /GRANULARITY
            else:
                if table[i][j]-table[i][j-1] != 0 or (j -1) in time_array:
                    table_out[i][j] = table[i][j] / GRANULARITY
    writecsv(table_out, filename)
    

def initial_stepwise(filename, GRANULARITY, day, PATH_FOLDER_SCHEDULER_DATA):
    out, row, col = readcsv(filename)
    for i in range(1, row):
        out[i, 1:], a, b, c = Stepwise(out[i, 1:], GRANULARITY)

    for i in range(1, row):
        for j in range(1, col):
            out[i][j] = out[i][j] / GRANULARITY
    output_file=PATH_FOLDER_SCHEDULER_DATA+'initial_average_wav_sequence_'+str(day)+'.csv'
    writecsv(out, output_file)



def main(PATH_FOLDER_OFFLINE_DEMAND_DATA, PATH_FOLDER_SCHEDULER_DATA):
    # function inputs
    
    # GRANULARITY = 10240  # Mbps   wavelength capacity
    # OVERALL_EFF_THRESHOLD =  0.82

    # # bandwidth efficiency
    # INDIV_EFF_THRESHOLD = 0.81
    # # this number must be smaller than the efficiency after scheduler
    # AGG_EFF_THRESHOLD = 0.80

    # PATH_FOLDER_OFFLINE_DEMAND_DATA = '../../data/generated_data/offline_demand_milan/'
    # PATH_FOLDER_SCHEDULER_DATA = '../../data/generated_data/scheduler/'

    if not os.path.exists(PATH_FOLDER_SCHEDULER_DATA):
        os.makedirs(PATH_FOLDER_SCHEDULER_DATA)

    for day in range(1,NUMBER_OFFLINE_DAYS+1):#63
        filename = PATH_FOLDER_OFFLINE_DEMAND_DATA+'offline_demand_data_'+str(day)+'.csv'
        initial_stepwise(filename, GRANULARITY, day, PATH_FOLDER_SCHEDULER_DATA)
           
        recon_table, states_table, full_table = global_scheduler(filename, GRANULARITY, OVERALL_EFF_THRESHOLD, INDIV_EFF_THRESHOLD)

        change = recon_table
        size_row1, size_col1 = change.shape
        states = states_table
        size_row2, size_col2 = states.shape
        full_states = full_table
        size_row3, size_col3 = full_table.shape
        cluster_table, size_row4, size_col4 = readcsv(filename)

        total_demand = sum(sum(cluster_table[1:size_row4,1:size_col4]))

        # greedy procedure to find number of time points and corresponding time array
        print "-----------------------greedy-----------------------"
        start_time = float(time.time())

        for num_time in range(1, 25):
            time_array, loss, table_out = AggregationGreedy(num_time, full_states)
        #print total_demand/sum(sum(table_out[1:,1:]))
            if total_demand/sum(sum(table_out[1:,1:])) >= AGG_EFF_THRESHOLD:
                #print total_demand/sum(sum(table_out[1:,1:]))
                break

       
        end_time = float(time.time())
        print "time consumption of SA is %f ms" %((end_time - start_time)*10**3 )
        print "-----------------------------------------------------"
        print "If the bandwidth efficiency requirement is %f, then number of reconfiguration time points should be %d" \
        %(AGG_EFF_THRESHOLD, num_time)
        print "aggregation time points after greedy is: ", time_array
        print "allocation loss by greedy method is %f TB" %(loss/8*3600/(1204**2))
        
        name=PATH_FOLDER_SCHEDULER_DATA+'change_greedy_'+str(day)+'.csv'
        write_change(table_out, name, GRANULARITY)
        name= PATH_FOLDER_SCHEDULER_DATA+'states_greedy_'+str(day)+'.csv'
        write_states(table_out, name, GRANULARITY, time_array)
        
        ## array_initial = InitialArray(num_time)
        # Simulated annealing procedure to search a better solution
        # intial solution of SA is the result of greedy
        array_initial = time_array
        time_array, loss, table_SA = SimulatedAnnealing(num_time, array_initial, full_states)
        #name= '../../generated_dataset/scheduler/time_array_SA_'+str(day)+'.pkl'
        pickle.dump(time_array, open(PATH_FOLDER_SCHEDULER_DATA+'time_array_SA_'+str(day)+'.pkl', 'wb'))
        
        sio.savemat(PATH_FOLDER_SCHEDULER_DATA+'time_array_SA_'+str(day)+'.mat', {'time_array':time_array})
        
        print "aggregation time points after SA is: ", time_array
        print "allocation loss by SA method is %f TB" %(loss/8*3600/(1204**2))
        name=PATH_FOLDER_SCHEDULER_DATA+'change_SA_'+str(day)+'.csv'
        write_change(table_SA, name, GRANULARITY)
        name=PATH_FOLDER_SCHEDULER_DATA+'states_SA_'+str(day)+'.csv'
        write_states(table_SA, name, GRANULARITY, time_array)
        
        print "--------------bandwith-allocation--------------------"
        print "total demand is %f TB" %(total_demand/8*3600/(1204**2))
        alloc = sum(sum(full_states[1:,1:]))
        print "total allocation before aggregation is %f TB" %(alloc/8*3600/(1204**2))
        alloc_greedy = sum(sum(table_out[1:,1:]))
        print "total allocation after greedy is %f TB" %(alloc_greedy/8*3600/(1204**2))
        eff_greedy = total_demand/alloc_greedy
        print "overall efficiency after greedy is %f" %(eff_greedy)
        alloc_SA = sum(sum(table_SA[1:,1:]))
        print "total allocation after simulated annealing is %f TB" %(alloc_SA/8*3600/(1204**2))
        eff_SA = total_demand/alloc_SA
        print "overall efficiency after SA is %f" %(eff_SA)

    
if __name__ == '__main__':

    if (len(sys.argv))>=3:
        PATH_FOLDER_OFFLINE_DEMAND_DATA = sys.argv[1]
        PATH_FOLDER_SCHEDULER_DATA = sys.argv[2]

    main(PATH_FOLDER_OFFLINE_DEMAND_DATA, PATH_FOLDER_SCHEDULER_DATA)
