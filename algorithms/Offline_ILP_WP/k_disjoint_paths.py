#!/usr/bin/env python
'''Compute the K edge disjoint paths between src and dst for a Undirected and weighted graph.

From Survivable Networks: Algorithms for Diverse Routing by Ramesh Bhandari 

See also:
http://en.wikipedia.org/wiki/Edge_disjoint_shortest_pair_algorithm

@author: Rodolfo Alvizu
'''
import sys

import networkx as nx

from lib.graph import interlacing_edges, pathlen, edges_on_path
from lib.graph import increase_and_negate_paths

# From http://docs.python.org/library/sys.html:
# max    DBL_MAX    maximum representable finite float
INF = sys.float_info.max


def get_INF2(g):
    '''Calculation of a large number.
    
    Chapter 4.1, Pg. 94 in Survivable networks (Bhandari)
    
    The algorithims to find k shortests paths requires the removal of the 
    forward arcs of the shortest path edges which are in the direction: 
    source vertex to destination vertex.
    In practice, instead of removing these arcs,it is simply convinient to replace
    their lengths by a large nuber simulating "infinity" so these arcs are never 
    traversed by the shortest path sought in the modified graph.
    
    INF2 = L + e
    where L is the sumation of all arcs in the graphs and
    e is an arbitrary positive number to cover the possibility of a graph in which L=0
    
    @param g: NetworkX Graph object
    @return INF2: large nuber simulating "infinity"
    
    '''
    INF2 = 50
    for a,b in g.edges():
        INF2 += g[a][b]['weight']
    
    return INF2 


def BFS(g, src, dst):
    '''BFS shortest path algorithm.

    Chapter 2.4, Pg. 33 in SN: ADR (see above)

    @param g: NetworkX Graph
    @param src: source node
    @param dst: destination node
    @return path: list of nodes in shortest path from src to dst, or None if 
    no path exists.
    '''
    d = {}  # d[i] is distance of vertex i (in V) from source vertex A.  It
            # is the sum of arcs in a possible path from vertex A to vertex i.
    P = {}  # P[i] is predecessor of vertex i on the same path.
    A = src
    Z = dst
    V = g.nodes()

    def l(a, b):
        return g[a][b]['weight']

    # Step 1:
    # Start with:
    #  d(A)  = 0
    d[A] = 0.0
    #  d(i) = INF for all i in V (i != A)
    for i in V:
        if i != A:
            d[i] = INF
    # Assign P(i) = A for all i in V (i != A)
    for i in V:
        if i != A:
            P[i] = A
    # Let Gamma^T denote the set of vertices from which search or scanning
    # (fanning out) takes place in a given iteration, and let GammaI denote
    # the set of vertices whose labels are updated in that iteration.
    # Gamma_J as before denotes the set of neighbor vertices of vertex j.  Start
    # with GammaT = {A}.
    GammaT = set([])  # Set of vertices from which search or scanning
                      # (fanning out) takes place in a given iteration
    GammaI = set([])  # set of vertices whose labels are updated in that
                      # iteration.
    GammaT.add(A)

    while True:
        # Step 2:
        # Set GammaI = {null set}
        GammaI = set([])
        # For all j in GammaT
        for j in GammaT:
            # do the following:
            # For all i in GammaJ:
            for i in g.neighbors(j):
                # if d(j) + l(ji) < d(i) and d(j) + l(ji) < d(Z):
                #   set d(i) = d(j) + l(ji)
                #       P(i) = j
                #       GammaI = GammaI union {i}
                if d[j] + l(j, i) < d[i] and d[j] + l(j, i) < d[Z]:
                    d[i] = d[j] + l(j, i)
                    P[i] = j
                    GammaI.add(i)
        # Set GammaT = GammaI - (GammaI intersect {Z}).
        GammaT = GammaI - (GammaI & set([Z]))
        
        # Step 3:
        # If GammaT = {null set}, END;
        if GammaT == set([]):
            break
        else:
            # otherwise go to 2.
            pass

    # Recover & return path
    if P[Z] == A:
        if g.has_edge(A, Z):
            return [A, Z]
        else:
            # Didn't find a path :-(
            return None
    else:
        pred = P[Z]
        path = [Z]
        while pred != A:
            path.append(pred)
            pred = P[pred]
        path.append(pred)
        path.reverse()
        return path


def grouped_shortest_paths(g, new_shortest_path, P_current,k):
    '''Find the k edge-disjoint paths from possibly interlacing paths.
    Remove any interlacing_edges() from the set P and the new_shortest_path.
    Interlacing edges are those whith overlapping edges that are traversed by
    two paths in the opposite direction.

    @param g: original NetworkX Graph or DiGraph
    @param new_shortest_path: list of path nodes
    @param P_current: current set of k edge-disjoint paths (list of path nodes)
    @return P_new: new set of k+1 edge-disjoint paths (list of path nodes)
    '''
    src = new_shortest_path[0]
    dst = new_shortest_path[-1]
    assert src == P_current[0][0]
    assert dst == P_current[0][-1]

    g3 = nx.Graph()
    g3.add_path(new_shortest_path)
    for a, b in edges_on_path(new_shortest_path):
        g3[a][b]['weight'] = g[a][b]['weight']
    
    for path in P_current:
        g3.add_path(path)
        # copy edges on path:
        for a, b in edges_on_path(path):
            g3[a][b]['weight'] = g[a][b]['weight']
        
    for path in P_current:
        for a, b in interlacing_edges(path, new_shortest_path):
            g3.remove_edge(a, b)

    # Return all the edge disjoint paths in g3
    # Find a path through graph and remove edges used.
    P_new = []
    for n in range(k):
        P_new.append(BFS(g3, src, dst))
        for a, b in edges_on_path(P_new[n]):
            g3.remove_edge(a, b)
        
    assert g3.number_of_edges() == 0
    
    return P_new


def k_edge_disjoint_paths(g, src, dst):
    '''Return list of k edge-disjoint paths w/shortest total cost.
    
    Chapter 4.1, Pg. 94 in Survivable networks (Bhandari)
    
    Simmilar to the k=2 case, the k>2 disjoint paths are obtained from k 
    iterations of themodified Dijkstra or the BFS algorithm in a graph
    appropriately modified at the end of each iteration.
    
    Algorithm created from algorithm 4.1 (Pg. 96) and Section 7.1 (Pg.175):
    
    1. For the pair src,dst find the shortest path using the modified Dijkstra
       or the BFS algorithm. 
       Introduce the path in the set P.
       Denote the lengh of the path by d_src_dst.
    
    2. Increment the length of each arc in the shortest path set P by INF2.
    
    3. Make the oppositely directed arcs of those in the shortest path set P 
       (from dst to src) Negative.
    
    4. Run the modified Dijkstra or BFS algorithm to find a new shortest path in
       the modified graph; denote the length of the path by d'_src_dst.
    
    5. If d'_src_dst >= INF2 :
            a new edge-disjoint path does not exist.
            Finish the algorithm: Return the set of edge-disjoint paths.
    
    6. Transform to the original graph. 
       Remove any interlacing_edges() from the set P and the new shortest path 
       founded in step 4.
       Interlacing edges are those whith overlapping edges that are traversed by
       two paths in the opposite direction.
    
    7. Update the set of shortes paths P.
    
    8. Go to step 2.
    
    @param g: NetworkX Graph object
    @param src: src node label
    @param dst: dst node label
    @return P: set of edge-disjoint paths [[Src,A,B,C,Dst],...[Src,C,D,Dst]]
    @return K: number of founded edge-disjoint paths
    '''
    INF2 = get_INF2(g)
    
    # Create the set of paths P and set of costs C
    P = []
    
    # 1. Use BFS to get shortest path.
    shortest_path = BFS (g, src, dst)
    P.append(shortest_path)
    k=1
    while True:
        # Modify the weight of each edge of the shortest path (equivalent to
        # two oppositely directed arcs) by:
        # 2. Increase by INF2 the weight of the forward arcs in the set P
        # 3. Negate the length of backwards arcs (directed toward the source) in
        # the set P. 
        g2 = increase_and_negate_paths(g,P,INF2)
    
        # 4. Run the modified Dijkstra or the BFS on the modiffied graph g2.
        shortest_path = BFS(g2, src, dst)
        
        # 5. If the length of the shortest_path is >= INF2 a new edge-disjoint 
        # path does not exist. Finish the algorithm: Return the set P
        if pathlen(g2,shortest_path) >= INF2 :
            break
        
        # 6. Transform to the original graph, and erase any interlacing edges of
        # the two paths found.  Group the remaining edges to obtain the shortest
        # pair of edge-disjoint paths.
        
        # 7. Update P
        k+=1
        first_pathtotal =  pathlen(g2, shortest_path) # the lengh of all paths 
                                                      #should be the same before
                                                      #and after using grouped_shortest_paths
        for paths in P:
            first_pathtotal += pathlen(g, paths)
        
        P = grouped_shortest_paths(g, shortest_path, P,k)

        second_pathtotal = 0
        for paths in P:
            second_pathtotal += pathlen(g, paths)
        
        
        assert(first_pathtotal == second_pathtotal),\
            "%r != %r , K= %r edge_disjoint paths" % (first_pathtotal,second_pathtotal,k)
        
    return P, k # returns a tuple