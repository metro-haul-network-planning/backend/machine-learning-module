#!/bin/sh

# BRIAN
# max = 17662
max=10
cell="MultiRNN"

for i in `seq 0 $max`
do
	echo "10,10,10,10,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50,50,50,50,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100,100,100,100,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

cell="MultiLSTM"

for i in `seq 0 $max`
do
	echo "10,10,10,10,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50,50,50,50,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100,100,100,100,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

cell="MultiGRU"

for i in `seq 0 $max`
do
	echo "10,10,10,10,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50,50,50,50,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100,100,100,100,1: COUPLE OF NODES $i"
	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
done



# for i in `seq 0 $max`
# do
# 	echo "10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,100,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,200,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,500,1 $cell "$i"
# done

# #############################################################################################

# cell="MultiLSTM"

# for i in `seq 0 $max`
# do
# 	echo "10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,100,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,200,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,500,1 $cell "$i"
# done


# #############################################################################################

# cell="MultiGRU"

# for i in `seq 0 $max`
# do
# 	echo "10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "10,10,10,10,10,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 10,10,10,10,10,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100,100,100,100,100,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 100,100,100,100,100,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200,200,200,200,200,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 200,200,200,200,200,1 $cell "$i"
# done


# for i in `seq 0 $max`
# do
# 	echo "300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300,300,300,300,300,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 300,300,300,300,300,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400,400,400,400,400,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 400,400,400,400,400,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,1 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500,500,500,500,500,1: COUPLE OF NODES $i"
# 	python BRIAN_model_training_Multi.py Data/X_brian_new.npy 500,500,500,500,500,1 $cell "$i"
# done