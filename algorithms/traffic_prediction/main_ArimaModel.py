import PredictionClass


data = 4

ArimaModel = PredictionClass.ArimaPredictionModel(
	p_train=0.99,
	p=10,
	q=2,
	m=0)

if data == 4:

    # ABILENE NETWORK, a prediction model for eah couple of nodes

	path = 'Data/3_Big_Data_Challenge_Milan/cdr_milan_matrix_internet.csv'
    # N0 -> N1 couple_nodes = 0
    # N0 -> N2 couple_nodes = 1
    # N0 -> N3 couple_nodes = 2
    # N0 -> N4 couple_nodes = 3
    # N0 -> N5 couple_nodes = 4
    # ecc
	couple_nodes = 0 
	ArimaModel.data_processing_5(path, couple_nodes)
	path_model = 'ArimaModel_'
	path_results = 'ArimaModel'
	ArimaModel.TrainTest(path_model, path_results)
