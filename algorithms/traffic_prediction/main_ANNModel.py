import PredictionClass
import tensorflow as tf


ANNModel = PredictionClass.ANNPredictionModel(
    n_neurons_per_layer=[100, 50, 10],
    num_inputs = 1,
    num_outputs = 1,    
    learning_rate=0.01,
    p_train=60, 
    p_validation=30, 
    p_test=10,
    num_train_iterations=1000,
    normalization=1,
    activation_function=tf.nn.relu,
    activate_dropout = 0.5,
    activate_EAM = 1,
    thr_iteration = 100
    )

path = 'Data/3_Big_Data_Challenge_Milan/data_ANN/features_cell382_weekdays.csv'
ANNModel.data_processing_5(path)

path_model = 'ann_model_1'
path_results = 'ann_model_1_results'
ANNModel.train(path_model, path_results)