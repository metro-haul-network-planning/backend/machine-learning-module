#!/bin/sh


# BRIAN
# max = 17662
max=10
cell="SingleRNN"

for i in `seq 0 $max`
do
	echo "10: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
done

cell="SingleLSTM"

for i in `seq 0 $max`
do
	echo "10: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
done

cell="SingleGRU"

for i in `seq 0 $max`
do
	echo "10: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "50: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
done

for i in `seq 0 $max`
do
	echo "100: COUPLE OF NODES $i"
	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
done

# for i in `seq 0 $max`
# do
# 	echo "10: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "20: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 20 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "30: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 30 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "40: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 40 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "50: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "60: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 60 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "70: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 70 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "80: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 80 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "90: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 90 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "150: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 150 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 200 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "250: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 250 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 300 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "350: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 350 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 400 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "450: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 450 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "600: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 600 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "700: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 700 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "800: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 800 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "900: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 900 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1000 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "2000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 2000 $cell "$i"
# done

# #########################################################################

# cell="SingleLSTM"

# for i in `seq 0 $max`
# do
# 	echo "10: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "20: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 20 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "30: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 30 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "40: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 40 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "50: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "60: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 60 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "70: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 70 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "80: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 80 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "90: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 90 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "150: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 150 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 200 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "250: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 250 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 300 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "350: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 350 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 400 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "450: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 450 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "600: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 600 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "700: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 700 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "800: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 800 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "900: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 900 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1000 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "2000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 2000 $cell "$i"
# done

# ######################################################################

# cell="SingleGRU"

# for i in `seq 0 $max`
# do
# 	echo "10: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 10 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "20: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 20 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "30: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 30 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "40: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 40 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "50: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 50 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "60: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 60 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "70: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 70 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "80: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 80 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "90: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 90 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "100: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 100 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "150: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 150 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "200: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 200 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "250: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 250 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "300: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 300 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "350: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 350 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "400: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 400 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "450: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 450 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "600: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 600 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "700: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 700 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "800: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 800 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "900: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 900 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1000 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "1500: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 1500 $cell "$i"
# done

# for i in `seq 0 $max`
# do
# 	echo "2000: COUPLE OF NODES $i"
# 	python BRIAN_model_test.py Data/X_brian_new.npy 2000 $cell "$i"
# done