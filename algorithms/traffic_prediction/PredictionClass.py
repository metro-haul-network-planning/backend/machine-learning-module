import tensorflow as tf
import numpy as np
import pandas as pd
import os
import gym
from sklearn.preprocessing import MinMaxScaler
from time import sleep
import shutil
import matplotlib.animation as animation
from sklearn.metrics import mean_absolute_error, mean_squared_error 
from math import sqrt
from matplotlib import pyplot
from pandas.tools.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.model_selection import train_test_split
import time
import keras 
from keras.models import *
from keras.layers import *

class RNNPredictionModel:

	def __init__(self, 
				n_neurons_per_layer=[100, 1],
				num_neurons = 100,
				num_inputs = 1,
				num_outputs = 1,    
				learning_rate=0.01,
				p_train=60, 
				p_validation=30, 
				p_test=10,
				num_train_iterations=1000,
				normalization=1,
				activation_function=tf.nn.relu,
				type_cell='SingleLSTM',
				activate_dropout = 1,
				activate_EAM = 1,
				thr_iteration = 100,
				):

		# Number of neurons per hidden layer
		self.n_neurons_per_layer = n_neurons_per_layer
		# Number of neurons single layer
		self.num_neurons = num_neurons
		# Number of inputs
		self.num_inputs = num_inputs
		# Number of putputs in single layer mode
		self.num_outputs = num_outputs
		# Learning rate
		self.learning_rate = learning_rate
		# percentage of training
		self.p_train = p_train 
		# percentage of validation
		self.p_validation = p_validation 
		# percentage of test
		self.p_test = p_test 
		# how many iterations to go through (training steps), you can play with this
		self.num_train_iterations = num_train_iterations
		# Normalize the dataset = 1, otherwise 0
		self.normalization = normalization
		# Activation function
		self.activation_function = activation_function
		# Type of cell
		self.type_cell = type_cell
		# Dropout 1 yes, 0 otherwise
		self.activate_dropout = activate_dropout
		# Activate the EAM module=1, 0 otherwise
		self.activate_EAM = activate_EAM
		# Threshold iteration for EAM algorithm
		self.thr_iteration = thr_iteration

		##########################################
		# INTERNAL PARAMETERS

		# EAM initialization parameters
		self.FLAG = 0
		self.iteration_count = 0
		self.generalized_model_iteration = 0
		# Datavisualization parameter
		self.FLAG_2 = 0
		# Training test parameter 
		self.FLAG_3 = 1



	# Load data functions. There is a function for each use case.
	# 1 = 1_Abilene_Network
	# 2 = 2_Network_Slicing_Milan_Network

	def make_timeseries_instances(self, timeseries, window_size):
		"""Make input features and prediction targets from a `timeseries` for use in machine learning.
		:return: A tuple of `(X, y, q)`.  `X` are the inputs to a predictor, a 3D ndarray with shape
		  ``(timeseries.shape[0] - window_size, window_size, timeseries.shape[1] or 1)``.  For each row of `X`, the
		  corresponding row of `y` is the next value in the timeseries.  The `q` or query is the last instance, what you would use
		  to predict a hypothetical next (unprovided) value in the `timeseries`.
		:param ndarray timeseries: Either a simple vector, or a matrix of shape ``(timestep, series_num)``, i.e., time is axis 0 (the
		  row) and the series is axis 1 (the column).
		:param int window_size: The number of samples to use as input prediction features (also called the lag or lookback).
		"""
		timeseries = np.asarray(timeseries)
		assert 0 < window_size < timeseries.shape[0]
		X = np.atleast_3d(np.array([timeseries[start:start + window_size] for start in range(0, timeseries.shape[0] - window_size)]))
		y = timeseries[window_size:]
		#q = np.atleast_3d([timeseries[-window_size:]])
		return X, y #, q


	def data_processing_CNN(self, path):

		data = np.load(path)
		window_size = 6

		train_percentage = 0.7
		validation_percentage = 0.9

		dim_data = data.shape[0]

		
		train_set = data[0:int(dim_data*train_percentage)]
		validation_set = data[int(dim_data*train_percentage):int(dim_data*validation_percentage)]
		test_set = data[int(dim_data*validation_percentage):]

		self.x_tr, self.y_tr = self.make_timeseries_instances(train_set,window_size)
		self.x_va, self.y_va = self.make_timeseries_instances(validation_set,window_size)
		self.x_te, self.y_te = self.make_timeseries_instances(test_set,window_size)


	def data_processing_6(self, path, couple_nodes):
		# X_brian

		self.num_time_steps = 92+1
		# Manual shift in case the time series must follow a fixed dimension as input (see the first use case as example)
		self.shift = 1

		X = np.load(path)
		data_1 = pd.DataFrame(X[:,couple_nodes])
		self.data = data_1
		# pyplot.plot(self.data)
		# pyplot.show()

		# Number of train, validation, test samples
		len_data = len(self.data)
		self.n_train = int(len_data*self.p_train/100)+1   
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   
		self.n_test = len_data_2-self.n_validation 
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print '----------------------'

		self.train_set = self.data[0:self.n_train]
		## print self.train_set.shape
		self.validation_set = self.data[self.n_train:(self.n_train+self.n_validation)]
		self.test_set = self.data[(self.n_train+self.n_validation):(self.n_train+self.n_validation+self.n_test)]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)


	def data_processing_1(self, path):

		# Initial settings
		# dimension of traffin matrix serialized
		self.tm_vect_dim=132 
		# Num of steps in each batch
		self.num_time_steps = 6*132
		self.shift = 132

		# Data input Abilene network
		# print('Data input')
		data_1 = pd.read_csv(path)
		data_1.index = data_1.timestamp
		data_1 = pd.DataFrame(data_1.value)
		self.data = data_1
		# print('----------------------')
		# print('Data samples:')
		# print(self.data.head())
		# print('----------------------')

		# Number of train, validation, test samples
		len_data = len(self.data)/self.tm_vect_dim
		self.n_train = int(len_data*self.p_train/100)+1   #2246
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   #561
		self.n_test = len_data_2-self.n_validation #401
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print 'Traffic Matrix dimension: ', self.tm_vect_dim
		# print '----------------------'

		self.train_set = self.data.iloc[0:self.n_train*self.tm_vect_dim]
		# # print self.train_set.shape
		self.validation_set = self.data.iloc[self.n_train*self.tm_vect_dim:(self.n_train+self.n_validation)*self.tm_vect_dim]
		self.test_set = self.data.iloc[(self.n_train+self.n_validation)*self.tm_vect_dim:(self.n_train+self.n_validation+self.n_test)*self.tm_vect_dim]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)

	def data_processing_2(self, path, chain_ID):

		# Initial settings
		# Num of steps in each batch
		self.num_time_steps = 92+1
		# Manual shift in case the time series must follow a fixed dimension as input (see the first use case as example)
		self.shift = 1

		# print('Data input')
		data_1 = pd.read_csv(path, index_col=0)
		data_1 = data_1.T
		data_1 = data_1.iloc[:, chain_ID-1:chain_ID]
		self.data = data_1
		
		# print('----------------------')
		# print 'Data samples:' 
		# print self.data.head()
		# print('----------------------')

		# Number of train, validation, test samples
		len_data = len(self.data)
		self.n_train = int(len_data*self.p_train/100)+1   
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   
		self.n_test = len_data_2-self.n_validation 
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print '----------------------'

		self.train_set = self.data.iloc[0:self.n_train]
		## print self.train_set.shape
		self.validation_set = self.data.iloc[self.n_train:(self.n_train+self.n_validation)]
		self.test_set = self.data.iloc[(self.n_train+self.n_validation):(self.n_train+self.n_validation+self.n_test)]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)

	def data_processing_3(self, path):

		# Initial settings
		# Input dimension
		self.num_inputs = 1
		# Output dimension
		self.num_outputs = 1
		# Num of steps in each batch
		self.num_time_steps = 48+1
		# Manual shift in case the time series must follow a fixed dimension as input (see the first use case as example)
		self.shift = 1

		# print('Data input')
		# Inserisci qua il processing che fai ai dati di caching.
		########################################################



		########################################################

		# print('----------------------')
		# print 'Data samples:' 
		# print self.data.head()
		# print('----------------------')

		# Number of train, validation, test samples
		len_data = len(self.data)
		self.n_train = int(len_data*self.p_train/100)+1   
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   
		self.n_test = len_data_2-self.n_validation 
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print '----------------------'

		self.train_set = self.data.iloc[0:self.n_train]
		## print self.train_set.shape
		self.validation_set = self.data.iloc[self.n_train:(self.n_train+self.n_validation)]
		self.test_set = self.data.iloc[(self.n_train+self.n_validation):(self.n_train+self.n_validation+self.n_test)]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)

	def data_processing_4(self, path, couple_nodes):

		# Initial settings 
		# Num of steps in each batch
		self.num_time_steps = 72+1
		self.shift = 1

		# Data input Abilene network
		# print('Data input')
		data_1 = pd.read_csv(path)
		data_1.index = data_1.timestamp
		data_1 = pd.DataFrame(data_1.value)
		# Insert the for to extract the couple of nodes
		vect_dim = 132
		data_temp = np.zeros(((len(data_1)/vect_dim)+1))
		ind_data = 0
		for ind in range(couple_nodes,len(data_1),vect_dim):
			temp = data_1.iloc[ind]
			data_temp[ind_data] = temp
			ind_data = ind_data + 1

		self.data = pd.DataFrame(data_temp)

		# print('----------------------')
		# print('Data samples:')
		# print(self.data.head())
		# print('----------------------')

		# Number of train, validation, test samples
		len_data = len(self.data)
		self.n_train = int(len_data*self.p_train/100)+1   #2246
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   #561
		self.n_test = len_data_2-self.n_validation #401
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print '----------------------'

		self.train_set = self.data.iloc[0:self.n_train]
		# # print self.train_set.shape
		self.validation_set = self.data.iloc[self.n_train:(self.n_train+self.n_validation)]
		self.test_set = self.data.iloc[(self.n_train+self.n_validation):(self.n_train+self.n_validation+self.n_test)]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)

	def data_processing_5(self, path, couple_nodes):

		# Initial settings 
		# Num of steps in each batch
		self.num_time_steps = 72+1
		self.shift = 1

		# Data input Abilene network
		# print('Data input')
		first_row = np.zeros((1488))
		data_1 = pd.read_csv(path, names=first_row)
		data_temp = data_1.iloc[couple_nodes]

		self.data = pd.DataFrame(data_temp)

		# print('----------------------')
		# print('Data samples:')
		# print(self.data.head())
		# print('----------------------')

		# Number of train, validation, test samples
		len_data = len(self.data)
		self.n_train = int(len_data*self.p_train/100)+1   #2246
		len_data_2 = len_data-self.n_train
		self.n_validation = int(len_data_2*self.p_validation/100)+1   #561
		self.n_test = len_data_2-self.n_validation #401
		tot = self.n_train+self.n_validation+self.n_test
		# print '----------------------'
		# print  'Total number of samples: ', len_data
		# print 'Samples for training: ', self.n_train, ', Samples for validation:', self.n_validation, ', Samples for testing the model:', self.n_test, ',Total:', tot
		# print '----------------------'

		self.train_set = self.data.iloc[0:self.n_train]
		# # print self.train_set.shape
		self.validation_set = self.data.iloc[self.n_train:(self.n_train+self.n_validation)]
		self.test_set = self.data.iloc[(self.n_train+self.n_validation):(self.n_train+self.n_validation+self.n_test)]

		if self.normalization == 0:
			self.train_scaled = self.train_set.values
			self.validation_scaled = self.validation_set.values
			self.test_scaled = self.test_set.values
		else:			
			# Scale the data
			self.scaler = MinMaxScaler()
			self.train_scaled = self.scaler.fit_transform(self.train_set)
			self.validation_scaled = self.scaler.transform(self.validation_set)
			self.test_scaled = self.scaler.transform(self.test_set)

	# -----------------------------------------------------------

	def next_batch(self, training_data, steps, shift):

		# Grab a random starting point for each batch
		rand_start = np.random.randint(0,len(training_data)-(steps+shift))

		# Create Y data for time series in the batches
		y_batch = np.array(training_data[rand_start:rand_start+steps+shift]).reshape(1,steps+shift)

		# return y_batch[:, :-shift].reshape(-shift, steps, 1), y_batch[:, shift:].reshape(-shift, steps, 1)
		return y_batch[:, :-shift].reshape(-1, steps, 1), y_batch[:, shift:].reshape(-1, steps, 1)

	# Functions for Convolutional layers

	def init_weights(self, shape):
		init_random_dist = tf.truncated_normal(shape, stddev=0.1)
		return tf.Variable(init_random_dist)

	def init_bias(self, shape):
		init_bias_vals = tf.constant(0.1, shape=shape)
		return tf.Variable(init_bias_vals)

	def conv2d(self, x, W):
		return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

	def max_pool_2by2(self, x):
		return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

	def convolutional_layer(self, input_x, shape):
		W = init_weights(shape)
		b = init_bias([shape[3]])
		return tf.nn.relu(conv2d(input_x, W) + b)

	def normal_full_layer(self, input_layer, size):
		input_size = int(input_layer.get_shape()[1])
		W = init_weights([input_size, size])
		b = init_bias([size])
		return tf.matmul(input_layer, W) + b

	def create_model(self):

		if self.type_cell == 'SingleGRU':
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])
			cells = tf.contrib.rnn.OutputProjectionWrapper(
				tf.contrib.rnn.GRUCell(num_units=self.num_neurons, activation=self.activation_function), output_size=self.num_outputs)
			# cells = tf.contrib.rnn.DropoutWrapper(cells)
			self.stacked_rnn_cell = cells
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)
			
		if self.type_cell == 'SingleLSTM':
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])
			cells = tf.contrib.rnn.OutputProjectionWrapper(
				tf.contrib.rnn.BasicLSTMCell(num_units=self.num_neurons, activation=self.activation_function), output_size=self.num_outputs)
			self.stacked_rnn_cell = cells
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)
			
		if self.type_cell == 'SingleRNN':
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])
			cells = tf.contrib.rnn.OutputProjectionWrapper(
				tf.contrib.rnn.BasicRNNCell(num_units=self.num_neurons, activation=self.activation_function), output_size=self.num_outputs)
			self.stacked_rnn_cell = cells
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)

		if self.type_cell == 'MultiGRU':		
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])	
			if self.activate_dropout == 0:
				cells = [tf.contrib.rnn.GRUCell(num_units=n, activation=self.activation_function) for n in self.n_neurons_per_layer]			
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cells)
			else:
				cell_tot = []
				for n in self.n_neurons_per_layer:
					cell = tf.contrib.rnn.GRUCell(num_units=n, activation=self.activation_function)
					cell_drop = tf.contrib.rnn.DropoutWrapper(cell)
					cell_tot.append(cell_drop)			
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cell_tot)
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)	
			
		if self.type_cell == 'MultiLSTM':
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])
			if self.activate_dropout == 0:
				cells = [tf.contrib.rnn.BasicLSTMCell(num_units=n, activation=self.activation_function) for n in self.n_neurons_per_layer]
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cells)
			else:
				cell_tot = []
				for n in self.n_neurons_per_layer:
					cell = tf.contrib.rnn.BasicLSTMCell(num_units=n, activation=self.activation_function)
					cell_drop = tf.contrib.rnn.DropoutWrapper(cell)
					cell_tot.append(cell_drop)			
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cell_tot)
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)	

		if self.type_cell == 'MultiRNN':
			self.X = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_inputs])
			self.y = tf.placeholder(tf.float32, [None, self.num_time_steps, self.num_outputs])
			if self.activate_dropout == 0:
				cells = [tf.contrib.rnn.BasicRNNCell(num_units=n, activation=self.activation_function) for n in self.n_neurons_per_layer]
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cells)
			else:
				cell_tot = []
				for n in self.n_neurons_per_layer:
					cell = tf.contrib.rnn.BasicRNNCell(num_units=n, activation=self.activation_function)
					cell_drop = tf.contrib.rnn.DropoutWrapper(cell)
					cell_tot.append(cell_drop)			
				self.stacked_rnn_cell = tf.contrib.rnn.MultiRNNCell(cell_tot)
			self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.X, dtype=tf.float32)
			self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
			self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
			self.train = self.optimizer.minimize(self.loss)	

	# Evaluation Automatic Module (EAM)
	
	def EAM(self, iteration):
		score = 1		
		if self.FLAG == 0:
			if self.MSE_validation[iteration] > self.MSE_validation[iteration-1]:
				# print 'Found first Generalized model at iteration: ', iteration-1, ' with MSE: ', self.MSE_validation[iteration-1]
				# Save iteration-1
				self.generalized_model_iteration = iteration-1
				self.FLAG = 1
		else:
			self.iteration_count = self.iteration_count + 1
			if self.iteration_count == self.thr_iteration:
				# print 'Reached ', self.thr_iteration, ' iterations'
				## print self.generalized_model_iteration
				## print iteration
				## print self.MSE_validation[self.generalized_model_iteration:iteration]
				val = min(self.MSE_validation)
				index = self.MSE_validation.index(val)

				# print 'Min value of the ', self.thr_iteration, ' more iterations: ', val
				# print 'Previous MSE: ', self.MSE_validation[self.generalized_model_iteration]

				if self.MSE_validation[self.generalized_model_iteration] > val:
					# print 'Found a better generalized model at iteration: ', index
					self.generalized_model_iteration = index
					score = 0
					self.FLAG = 0
				else:
					# print 'Keep the first generalized model'
					score = 0	
					self.FLAG = 0
					self.iteration_count = 0

		return score

	# Copy and paste the generalized model from the temp folder to the Model folder (in order to be used by the test function)
	def copy_paste(self, src, dst, iteration):
		
		basedir = src
		movdir = dst		

		filename1 = 'iteration_'+str(iteration)+'.data-00000-of-00001'
		filename2 = 'iteration_'+str(iteration)+'.index'
		filename3 = 'iteration_'+str(iteration)+'.meta'
		
		old_name1 = basedir+filename1
		old_name2 = basedir+filename2
		old_name3 = basedir+filename3

		new_name1 = movdir+'.data-00000-of-00001'
		new_name2 = movdir+'.index'
		new_name3 = movdir+'.meta'

		shutil.copy(old_name1, new_name1)
		shutil.copy(old_name2, new_name2)
		shutil.copy(old_name3, new_name3)

	def animation_files(self, var, title):
		
		folder = 'Results/visualization/'
		if not os.path.isdir(folder):
		    os.system('mkdir '+folder)
		path = folder+title

		if self.FLAG_2 == 0:
			f = open(path,'w')
			self.FLAG_2 = 1
		else:
			f = open(path,'a')
		f.write(str(var))
		f.write('\n')
		f.close()

	def delete_temp_files(self, path):

		folder = path
		for the_file in os.listdir(folder):
			file_path = os.path.join(folder, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
				#elif os.path.isdir(file_path): shutil.rmtree(file_path)
			except Exception as e:
				print(e)
	

	def train(self, path_model):

		# Create folders
		if not os.path.isdir('Results/'):
			os.system('mkdir Results') 

		if not os.path.isdir('Results/RNNModel'):
			os.system('mkdir Results/RNNModel') 

		if not os.path.isdir('Results/RNNModel/'+path_model):
			os.system('mkdir Results/RNNModel/'+path_model) 

		if not os.path.isdir('Model/'):
			os.system('mkdir Model') 

		if not os.path.isdir('Model/RNNModel'):
			os.system('mkdir Model/RNNModel') 

		if not os.path.isdir('Model/RNNModel/'+path_model):
			os.system('mkdir Model/RNNModel/'+path_model) 

		if not os.path.isdir('Model/RNNModel/'+path_model+'/temp/'):
			os.system('mkdir Model/RNNModel/'+path_model+'/temp/') 

		path_temp_model = 'Model/RNNModel/'+path_model+'/temp/' 
		path_candidate_model = 'Model/RNNModel/'+path_model
		path_results_model = 'Results/RNNModel/'+path_model+'/'

		# TRAINING
		# print('Training')
		self.MSE_train = []
		self.MSE_validation = []
		with tf.Session() as sess:

            # Restore previous trained model and continue to train
			if os.path.isfile(path_candidate_model+'/'+path_model+'.meta'):

				self.create_model()
				self.FLAG_3 = 0

				init = tf.global_variables_initializer()
				sess.run(init)
				
				saver = tf.train.Saver(max_to_keep=10000)
				saver.restore(sess, path_candidate_model+'/'+path_model)

				# print 'Previous model found. MODEL RESTORED'

			else:

				self.create_model()
				self.FLAG_3 = 0

				init = tf.global_variables_initializer()
				saver = tf.train.Saver(max_to_keep=10000)

				sess.run(init)

				# print 'Previous model not found. MAKING A NEW ONE.' 

			for iteration in range(self.num_train_iterations):

				X_batch_train, y_batch_train = self.next_batch(self.train_scaled,self.num_time_steps,self.shift)
				X_batch_validation, y_batch_validation = self.next_batch(self.validation_scaled,self.num_time_steps,self.shift)

				sess.run(self.train, feed_dict={self.X: X_batch_train, self.y: y_batch_train})
				#sess.run(self.train, feed_dict={self.X: X_batch_validation, self.y: y_batch_validation})

				# Loss computation				
				mse_train = self.loss.eval(feed_dict={self.X: X_batch_train, self.y: y_batch_train})
				mse_validation = self.loss.eval(feed_dict={self.X: X_batch_validation, self.y: y_batch_validation})
				
				self.MSE_train.append(mse_train)
				self.MSE_validation.append(mse_validation)		
				self.animation_files(mse_validation, title='mse.txt')

				# Save the model at each iteration []
				saver.save(sess, path_temp_model+'iteration_'+str(iteration))
				
				# if iteration % 100 == 0:
					# print 'Iteration:',iteration 
					# print 'MSE Train:', mse_train
					# print 'MSE Validation:', mse_validation

				if iteration > 0 and self.activate_EAM == 1:
					score = self.EAM(iteration)

					if score == 0:
						# print 'Genralized model saved at Iteration: ', self.generalized_model_iteration
						self.copy_paste(src=path_temp_model, dst=path_candidate_model+'/'+path_model, iteration=self.generalized_model_iteration)
						self.delete_temp_files('Model/RNNModel/'+path_model+'/temp')
						df_MSE_train = pd.DataFrame(self.MSE_train, columns=['MSE_train'])
						df_MSE_validation = pd.DataFrame(self.MSE_validation, columns=['MSE_validation'])

						if self.activate_dropout == 1:
							df_MSE_train.to_csv(path_results_model+'MSE_training_EAM_Drop.csv')
							df_MSE_validation.to_csv(path_results_model+'MSE_validation_EAM_Drop.csv')
						else:
							df_MSE_train.to_csv(path_results_model+'MSE_training_EAM_NoDrop.csv')
							df_MSE_validation.to_csv(path_results_model+'MSE_validation_EAM_NoDrop.csv')
						break
					
					if iteration == self.num_train_iterations-1:
						self.copy_paste(src=path_temp_model, dst=path_candidate_model+'/'+path_model, iteration=iteration)
						self.delete_temp_files('Model/RNNModel/'+path_model+'/temp')
						df_MSE_train = pd.DataFrame(self.MSE_train, columns=['MSE_train'])
						df_MSE_validation = pd.DataFrame(self.MSE_validation, columns=['MSE_validation'])

						if self.activate_dropout == 1:
							df_MSE_train.to_csv(path_results_model+'MSE_training_NoEAM_Drop.csv')
							df_MSE_validation.to_csv(path_results_model+'MSE_validation_NoEAM_Drop.csv')
						else:
							df_MSE_train.to_csv(path_results_model+'MSE_training_NoEAM_NoDrop.csv')
							df_MSE_validation.to_csv(path_results_model+'MSE_validation_NoEAM_NoDrop.csv')

				if iteration == self.num_train_iterations-1 and self.activate_EAM == 0:

					self.copy_paste(src=path_temp_model, dst=path_candidate_model+'/'+path_model, iteration=iteration)
					self.delete_temp_files('Model/RNNModel/'+path_model+'/temp')
					df_MSE_train = pd.DataFrame(self.MSE_train, columns=['MSE_train'])
					df_MSE_validation = pd.DataFrame(self.MSE_validation, columns=['MSE_validation'])

					if self.activate_dropout == 1:
						df_MSE_train.to_csv(path_results_model+'MSE_training_NoEAM_Drop.csv')
						df_MSE_validation.to_csv(path_results_model+'MSE_validation_NoEAM_Drop.csv')
					else:
						df_MSE_train.to_csv(path_results_model+'MSE_training_NoEAM_NoDrop.csv')
						df_MSE_validation.to_csv(path_results_model+'MSE_validation_NoEAM_NoDrop.csv')
					break


	def train_CNN(self, path_model):


		# Create folders
		if not os.path.isdir('Results/'):
			os.system('mkdir Results') 

		if not os.path.isdir('Results/RNNModel'):
			os.system('mkdir Results/RNNModel') 

		if not os.path.isdir('Results/RNNModel/'+path_model):
			os.system('mkdir Results/RNNModel/'+path_model) 

		if not os.path.isdir('Model/'):
			os.system('mkdir Model') 

		if not os.path.isdir('Model/RNNModel'):
			os.system('mkdir Model/RNNModel') 

		if not os.path.isdir('Model/RNNModel/'+path_model):
			os.system('mkdir Model/RNNModel/'+path_model) 

		if not os.path.isdir('Model/RNNModel/'+path_model+'/temp/'):
			os.system('mkdir Model/RNNModel/'+path_model+'/temp/') 

		path_temp_model = 'Model/RNNModel/'+path_model+'/temp/' 
		path_candidate_model = 'Model/RNNModel/'+path_model
		path_results_model = 'Results/RNNModel/'+path_model+'/'

		self.create_model()
		self.FLAG_3 = 0

		
		# saver = tf.train.Saver(max_to_keep=10000)

		# TRAINING
		# print('Training')
		self.MSE_train = []
		self.MSE_validation = []

		# print self.x_tr.shape, self.y_tr.shape

		self.X = tf.placeholder(tf.float32, [None, self.x_tr.shape[1], self.x_tr.shape[2]])
		self.y = tf.placeholder(tf.float32, [None, self.y_tr.shape[1]])

		self.conv = tf.layers.conv1d(inputs=self.X, filters=128, kernel_size=4, activation=tf.nn.relu)
		self.pool = tf.layers.max_pooling1d(inputs=self.conv, pool_size=2, strides=6)

		cells = tf.contrib.rnn.OutputProjectionWrapper(
			tf.contrib.rnn.BasicLSTMCell(num_units=self.num_neurons, activation=self.activation_function), output_size=self.num_outputs)
		self.stacked_rnn_cell = cells

		self.outputs, self.states = tf.nn.dynamic_rnn(self.stacked_rnn_cell, self.pool, dtype=tf.float32)
		self.loss = tf.reduce_mean(tf.square(self.outputs - self.y)) # MSE
		self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
		self.train = self.optimizer.minimize(self.loss)

		init = tf.global_variables_initializer()

		with tf.Session() as sess:
			
			sess.run(init)
			for iteration in range(self.num_train_iterations):
				# print 'Epoch: ', iteration 
				sess.run(self.train, feed_dict={self.X: self.x_tr, self.y: self.y_tr})


	def f(self, var):
		if isinstance(var, pd.DataFrame):
			return 0
		else:
			return 1

	def test(self, path_model, path_results):

		# TEST
		# print('Test')

		# Create folders
		if not os.path.isdir('Results/'):
			os.system('mkdir Results') 

		if not os.path.isdir('Results/RNNModel'):
			os.system('mkdir Results/RNNModel') 

		if not os.path.isdir('Results/RNNModel/'+path_model):
			os.system('mkdir Results/RNNModel/'+path_model) 

		path_candidate_model = 'Model/RNNModel/'+path_model
		path_results_model = 'Results/RNNModel/'+path_model+'/'

		if self.FLAG_3 == 1:
			self.create_model()		
		#init = tf.global_variables_initializer()	
		t_set = list(self.train_scaled[-self.num_time_steps:]) + list(self.test_scaled)

		index_left = 0
		index_right = self.num_time_steps
		y_pred = []
		saver = tf.train.Saver()
		with tf.Session() as sess:
			# Use your Saver instance to restore your saved rnn time series model
			#sess.run(init)
			#saver = tf.train.import_meta_graph(path_model+'.meta')
			saver.restore(sess, path_candidate_model+'/'+path_model)
			for i in range(self.n_test): #self.n_test
				
				X_batch = np.array(t_set[index_left+(i*self.shift):index_right+(i*self.shift)]).reshape(1, self.num_time_steps, 1)
				## print X_batch
				y_pred_TEMP = sess.run(self.outputs, feed_dict={self.X: X_batch})
				## print y_pred_TEMP.shape
				if i == 0:
					y_pred_2 = y_pred_TEMP[:, -self.shift:]
				else:
					y_pred_2 = np.concatenate((y_pred_2, y_pred_TEMP[:, -self.shift:]), axis = 1)

		if self.normalization == 0:
			predicted_test_set_array = np.array(y_pred_2).reshape(self.test_scaled.size ,1)
		else:
			predicted_test_set_array = self.scaler.inverse_transform(np.array(y_pred_2).reshape(self.test_scaled.size ,1))


		check_dataFrame = self.f(self.test_set)
		if check_dataFrame == 0:
			test_set_array = self.test_set.values
		else:
			test_set_array = self.test_set

		# Evaluation of the test error
		mae_array = np.zeros((1,1))
		mse_array = np.zeros((1,1))
		rmse_array = np.zeros((1,1))
		mae_array[0,0] = mean_absolute_error(test_set_array, predicted_test_set_array)
		mse_array[0,0] = mean_squared_error(test_set_array, predicted_test_set_array)
		rmse_array[0,0] = sqrt(mse_array[0,0])
		eva_results = np.concatenate((mae_array,mse_array,rmse_array), axis = 1)		
		df_eva_results = pd.DataFrame(eva_results, columns=['MAE', 'MSE', 'RMSE'])

		if os.path.isfile(path_results_model+'MAE_MSE_RMSE.csv'):
			df = pd.read_csv(path_results_model+'MAE_MSE_RMSE.csv')
			if df.empty == True:
				writing_mode = 'w'
				header_ = True
				with open(path_results_model+'MAE_MSE_RMSE.csv', writing_mode) as f:
					df_eva_results.to_csv(f, header=header_)		
			else:
				writing_mode = 'a'
				header_ = False
				with open(path_results_model+'MAE_MSE_RMSE.csv', writing_mode) as f:
					df_eva_results.to_csv(f, header=header_)	
		else:
			df_eva_results.to_csv(path_results_model+'MAE_MSE_RMSE.csv')

		# print 'Evaluation performance Test Set: MAE = '+str(mae_array[0,0])+' ; MSE = '+str(mse_array[0,0])+' RMSE = '+str(rmse_array[0,0])

		# results_concatenation = np.concatenate((test_set_array,predicted_test_set_array), axis = 1)
		# df_results = pd.DataFrame(results_concatenation, columns=['Test_Set', 'Predicted_Test_Set'])

		# if self.activate_dropout == 1 and self.activate_EAM == 1:
		# 	t = '_EAM_Drop'
		# if self.activate_dropout == 1 and self.activate_EAM == 0:
		# 	t = '_NoEAM_Drop'
		# if self.activate_dropout == 0 and self.activate_EAM == 1:
		# 	t = '_EAM_NoDrop'
		# if self.activate_dropout == 0 and self.activate_EAM == 0:	
		# 	t = '_NoEAM_NoDrop'

		# df_results.to_csv(path_results_model+path_results+t+'.csv')

		# Plot 
		# pyplot.plot(test_set_array)
		# pyplot.plot(predicted_test_set_array, color='red')
		# pyplot.show()


class ArimaPredictionModel:

	def __init__(self,
				p_train=0.70,
				p=3,
				q=2,
				m=0
				):

		self.p_train = p_train
		self.p = p
		self.q = q
		self.m = m

	def data_processing_5(self, path, couple_nodes):

		# Data input BIG data challenge network

		# print('Data input')
		first_row = np.zeros((1488))
		data_1 = pd.read_csv(path, names=first_row)
		data_temp = data_1.iloc[couple_nodes]

		self.series = pd.DataFrame(data_temp)

		# print('----------------------')
		# print('Data samples:')
		# print(self.series.head())
		# print('----------------------')

	def TrainTest(self, path_model, path_results):

		# Create Folders
		if not os.path.isdir('Results/'):
			os.system('mkdir Results') 

		if not os.path.isdir('Results/ArimaModel'):
			os.system('mkdir Results/ArimaModel') 

		if not os.path.isdir('Results/ArimaModel/'+path_model):
			os.system('mkdir Results/ArimaModel/'+path_model) 

		if not os.path.isdir('Model/'):
			os.system('mkdir Model') 

		if not os.path.isdir('Model/ArimaModel'):
			os.system('mkdir Model/ArimaModel') 

		if not os.path.isdir('Model/ArimaModel/'+path_model):
			os.system('mkdir Model/ArimaModel/'+path_model) 

		path_candidate_model = 'Model/ArimaModel/'+path_model
		path_results_model = 'Results/ArimaModel/'+path_model+'/'

		X = self.series.values
		size = int(len(X) * self.p_train)
		train, test = X[0:size], X[size:len(X)]
		history = [x for x in train]
		real = np.zeros((len(test),1))
		predicted = np.zeros((len(test),1))
		predictions = list()
		for t in range(len(test)):#len(test)
		    model = ARIMA(history, order=(self.p,self.q,self.m))
		    model_fit = model.fit(disp=0)
		    output = model_fit.forecast()
		    yhat = output[0]
		    predictions.append(yhat)
		    obs = test[t]
		    history.append(obs)
		    real[t,0] = obs
		    predicted[t,0] = yhat
		    # print('predicted=%f, expected=%f' % (yhat, obs))

		results_concatenation = np.concatenate((real,predicted), axis = 1)
		df_results = pd.DataFrame(results_concatenation, columns=['Test_Set', 'Predicted_Test_Set'])
		df_results.to_csv(path_results_model+path_results+'.csv')

		mae_array = np.zeros((1,1,))
		mse_array = np.zeros((1,1,))
		rmse_array = np.zeros((1,1,))
		mae_array[0,0] = mean_absolute_error(test, predictions)
		mse_array[0,0] = mean_squared_error(test, predictions)
		rmse_array[0,0] = sqrt(mse_array[0,0])
		eva_results = np.concatenate((mae_array,mse_array,rmse_array), axis = 1)
		df_eva_results = pd.DataFrame(eva_results, columns=['MAE', 'MSE', 'RMSE'])
		df_eva_results.to_csv(path_results_model+'MAE_MSE_RMSE.csv')

		# Plot
		pyplot.plot(test)
		pyplot.plot(predictions, color='red')
		pyplot.show()

		# print 'Evaluation performance Test Set: MAE = '+str(mae_array[0,0])+' ; MSE = '+str(mse_array[0,0])+' RMSE = '+str(rmse_array[0,0])


class ANNPredictionModel:

	def __init__(self, 
				n_neurons_per_layer=[100, 1],
				num_inputs = 1,
				num_outputs = 1,    
				learning_rate=0.01,
				p_train=60, 
				p_validation=30, 
				p_test=10,
				num_train_iterations=1000,
				normalization=1,
				activation_function=tf.nn.relu,
				activate_dropout = 1,
				activate_EAM = 1,
				thr_iteration = 100,
				):

		# Number of neurons per hidden layer
		self.n_neurons_per_layer = n_neurons_per_layer
		# Number of inputs
		self.num_inputs = num_inputs
		# Number of putputs in single layer mode
		self.num_outputs = num_outputs
		# Learning rate
		self.learning_rate = learning_rate
		# percentage of training
		self.p_train = p_train 
		# percentage of validation
		self.p_validation = p_validation 
		# percentage of test
		self.p_test = p_test 
		# how many iterations to go through (training steps), you can play with this
		self.num_train_iterations = num_train_iterations
		# Normalize the dataset = 1, otherwise 0
		self.normalization = normalization
		# Activation function
		self.activation_function = activation_function
		# Dropout 1 yes, 0 otherwise
		self.activate_dropout = activate_dropout
		# Activate the EAM module=1, 0 otherwise
		self.activate_EAM = activate_EAM
		# Threshold iteration for EAM algorithm
		self.thr_iteration = thr_iteration

		##########################################
		# INTERNAL PARAMETERS

		# EAM initialization parameters
		self.FLAG = 0
		self.iteration_count = 0
		self.generalized_model_iteration = 0
		# Datavisualization parameter
		self.FLAG_2 = 0
		# Training test parameter 
		self.FLAG_3 = 1

	def data_processing_5(self, path):

		# Data input BIG data challenge network

		data = pd.read_csv(path)
		# print data.head()

		data.describe().transpose()

		x_data = data.drop(['bias'],axis=1)
		x_data = data.drop(['Target'],axis=1)
		y_val = data['Target']
		self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(x_data,y_val,test_size=self.p_test/10,random_state=101)

		scaler = MinMaxScaler()
		scaler.fit(self.X_train)
		self.X_train = pd.DataFrame(data=scaler.transform(self.X_train),columns = self.X_train.columns,index=self.X_train.index)
		self.X_test = pd.DataFrame(data=scaler.transform(self.X_test),columns = self.X_test.columns,index=self.X_test.index)

		hour = tf.feature_column.numeric_column('hour')
		date = tf.feature_column.numeric_column('date')
		day = tf.feature_column.numeric_column('day')
		isWorking = tf.feature_column.numeric_column('isWorking')
		PreviousAvergarDay = tf.feature_column.numeric_column('PreviousAvergarDay')
		PreviousDay = tf.feature_column.numeric_column('PreviousDay')

		self.feat_cols = [hour,date,day,isWorking,PreviousAvergarDay,PreviousDay]


	def train(self, path_model, path_results):

		# Create folders
		if not os.path.isdir('Results/'):
			os.system('mkdir Results') 

		if not os.path.isdir('Results/ANNModel'):
			os.system('mkdir Results/ANNModel') 

		if not os.path.isdir('Results/ANNModel/'+path_model):
			os.system('mkdir Results/ANNModel/'+path_model) 

		if not os.path.isdir('Model/'):
			os.system('mkdir Model') 

		if not os.path.isdir('Model/ANNModel'):
			os.system('mkdir Model/ANNModel') 

		if not os.path.isdir('Model/ANNModel/'+path_model):
			os.system('mkdir Model/ANNModel/'+path_model) 

		if not os.path.isdir('Model/ANNModel/'+path_model+'/temp/'):
			os.system('mkdir Model/ANNModel/'+path_model+'/temp/') 	

		input_func = tf.estimator.inputs.pandas_input_fn(x=self.X_train,y=self.y_train ,batch_size=10,num_epochs=1000,
                                            shuffle=True)

		model = tf.estimator.DNNRegressor(
			hidden_units=self.n_neurons_per_layer,
			feature_columns=self.feat_cols,
			model_dir='Model/ANNModel/'+path_model+'/temp/',
			label_dimension=1,
			weight_column=None,
			optimizer='Adagrad',
			activation_fn=self.activation_function,
			dropout=self.activate_dropout,
			input_layer_partitioner=None,
			config=None,
			warm_start_from=None,
    		)

		predict_input_func = tf.estimator.inputs.pandas_input_fn(
			x=self.X_test,
			batch_size=10,
			num_epochs=1,
			shuffle=False)

		# mse_array = 
		for _ in range(self.num_train_iterations):
			
			model.train(input_fn=input_func,steps=1)
			# Check the model: EAM

			pred_gen = model.predict(predict_input_func)
			predictions = list(pred_gen)
			final_preds = []
			for pred in predictions:
				final_preds.append(pred['predictions'])

			# print mean_squared_error(self.y_test,final_preds)**0.5

class ConvRNN:

	def __init__(self):
		d = 0

	def make_timeseries_instances(self, timeseries, window_size):

		timeseries = np.asarray(timeseries)
		assert 0 < window_size < timeseries.shape[0]
		X = np.atleast_3d(np.array([timeseries[start:start + window_size] for start in range(0, timeseries.shape[0] - window_size)]))
		y = timeseries[window_size:]

		return X, y


	def train(self):

		path = 'Data/1_Abilene_Network/abilene_traffic_matrices.csv'
		timeseries = pd.read_csv(path)
		timeseries_array = timeseries.as_matrix(columns=['value'])

		X = np.reshape(timeseries_array, [len(timeseries_array)/132,132])
		window_size = 2
		X, Y = self.make_timeseries_instances(X, window_size)

		model = Sequential()
		model.add(Conv1D(filters=32,kernel_size=1, input_shape=(X.shape[1],X.shape[2]),activation='relu'))
		model.add(MaxPooling1D(2))
		# model.add(LSTM(30,return_sequences=True))
		model.add(LSTM(30,return_sequences=False))
		model.add(Dense(Y.shape[1], activation='sigmoid'))

		# compile model
		model.compile(loss='mse', optimizer='adam',metrics=['mae'])
		model.fit(X, Y, epochs=2000, shuffle=False, verbose=1)

		predicted = model.predict(X)
		# print mean_squared_error(Y,predicted)


# # Define Inputs
# inputs_ = tf.placeholder(tf.int32, [None, seq_len], name='inputs')
# labels_ = tf.placeholder(tf.float32, [None, 1], name='labels')
# training_ = tf.placeholder(tf.bool, name='training')

# # Define Embeddings
# embedding = tf.Variable(tf.random_uniform((vocab_size, embed_size), -1, 1))
# embed = tf.nn.embedding_lookup(embedding, inputs_)

# # Define Convolutional Layers with Max Pooling
# # convs = []
# # for filter_size in filter_sizes:
# conv = tf.layers.conv1d(inputs=self.X, filters=128, kernel_size=8, activation=tf.nn.relu)
# pool = tf.layers.max_pooling1d(inputs=conv, pool_size=2, strides=1)
# 	# convs.append(pool)

# # Concat Pooling Outputs and Flatten
# # pool_concat = tf.concat(convs, axis=-1)
# # pool_flat = tf.layers.Flatten(pool_concat)