import PredictionClass
import tensorflow as tf

# Choose Dataset:
# data = 1 -> Abilene Network
# data = 2 -> TIM mobile network
# data = 3 -> Cache content network

data = 6



if data == 1:

    # ABILENE NETWORK 

    path = 'Data/1_Abilene_Network/abilene_traffic_matrices.csv'
    RNNModel.data_processing_1(path)
    path_model = 'prediction_model_ABILENE'
    RNNModel.train(path_model)
    path_results = 'prediction_results_ABILENE'
    RNNModel.test(path_model, path_results)

if data == 2:

    # TIM MOBILE NETWORK

    path = 'Data/2_Network_Slicing_Milan_Network/trafficVolumesDataFrame.csv'
    chain_ID = 1
    RNNModel.data_processing_2(path, chain_ID)
    path_model = 'prediction_model_CHAIN_'+str(chain_ID)
    RNNModel.train(path_model)
    path_results = 'prediction_results_CHAIN_'+str(chain_ID)
    RNNModel.test(path_model, path_results)

if data == 3:

    # CACHE VIDEO CONTENT 

    path = 'Data/...' # Add file with data
    RNNModel.data_processing_3(path)
    path_model = 'prediction_model_CACHE'
    RNNModel.train(path_model)
    path_results = 'prediction_results_CACHE'
    RNNModel.test(path_model, path_results)


if data == 4:

    # ABILENE NETWORK, a prediction model for eah couple of nodes

    path = 'Data/1_Abilene_Network/abilene_traffic_matrices.csv'
    # N0 -> N1 couple_nodes = 0
    # N0 -> N2 couple_nodes = 1
    # N0 -> N3 couple_nodes = 2
    # N0 -> N4 couple_nodes = 3
    # N0 -> N5 couple_nodes = 4
    # ecc
    couple_nodes = 0 
    RNNModel.data_processing_4(path, couple_nodes)
    path_model = 'prediction_model_ABILENE_N2N'
    RNNModel.train(path_model)
    path_results = 'prediction_results_ABILENE_N2N'
    RNNModel.test(path_model, path_results)

if data == 5:

    # BIG DATA CHALLENGE, a prediction model for eah couple of nodes

    path = 'Data/3_Big_Data_Challenge_Milan/cdr_milan_matrix_internet.csv'
    # N0 -> N1 couple_nodes = 0
    # N0 -> N2 couple_nodes = 1
    # N0 -> N3 couple_nodes = 2
    # N0 -> N4 couple_nodes = 3
    # N0 -> N5 couple_nodes = 4
    # ecc
    couple_nodes = 0 
    RNNModel.data_processing_5(path, couple_nodes)
    path_model = 'prediction_model_BIG'
    RNNModel.train(path_model)
    path_results = 'prediction_results_BIG'
    RNNModel.test(path_model, path_results)

if data == 6:

    n_neurons = 10
    t_cell = 'SingleRNN'

    RNNModel = PredictionClass.RNNPredictionModel(
        n_neurons_per_layer = [100,10,1], # number of neurons per layer in case of multi hidden layer NN
        num_neurons = 10, # number of neurons in case of single hidden layer NN
        num_inputs = 1, # number of inputs 
        num_outputs = 1, # number of outputs in case of single hidden layer NN    
        learning_rate=0.001, # learning rate
        p_train=70, # percentage of training set
        p_validation=20, # percentage of validation set
        p_test=10, # percentage of testing set
        num_train_iterations=500,
        normalization=1, # 1 normalize, 0 otherwise
        activation_function=tf.nn.relu, # activation function: relu, elu
        type_cell='SingleRNN', # type of cell: SingleGRU, SingleLSTM, SingleRNN, MultiGRU, MultiLSTM, MultiRNN
        activate_dropout=1, # Dropout activate = 1, 0 otherwise
        activate_EAM=0, # activate EAM module = 1, 0 otherwise
        thr_iteration=100 # Number of iterations when EAM is activated
        )

    path = 'Data/X_brian.npy'
    couple_nodes = 35
    RNNModel.data_processing_6(path, couple_nodes)
    path_model = 'prediction_model_BRIAN_'+str(n_neurons)+'_'+t_cell+'_'+str(couple_nodes)
    RNNModel.train(path_model)
    path_results = 'prediction_results_BRIAN'+str(n_neurons)+'_'+t_cell+'_'+str(couple_nodes)
    RNNModel.test(path_model, path_results)


if data == 7:

    RNNModel = PredictionClass.RNNPredictionModel(
        n_neurons_per_layer = [100,10,1], # number of neurons per layer in case of multi hidden layer NN
        num_neurons = 100, # number of neurons in case of single hidden layer NN
        num_inputs = 1, # number of inputs 
        num_outputs = 1, # number of outputs in case of single hidden layer NN    
        learning_rate=0.001, # learning rate
        p_train=70, # percentage of training set
        p_validation=20, # percentage of validation set
        p_test=10, # percentage of testing set
        num_train_iterations=5,
        normalization=1, # 1 normalize, 0 otherwise
        activation_function=tf.nn.relu, # activation function: relu, elu
        type_cell='SingleRNN', # type of cell: SingleGRU, SingleLSTM, SingleRNN, MultiGRU, MultiLSTM, MultiRNN
        activate_dropout=1, # Dropout activate = 1, 0 otherwise
        activate_EAM=0, # activate EAM module = 1, 0 otherwise
        thr_iteration=100 # Number of iterations when EAM is activated
        )

    path = 'Data/X_abilene.npy'
    couple_nodes = 0 
    RNNModel.data_processing_6(path, couple_nodes)
    path_model = 'prediction_model_ABILENE'
    RNNModel.train(path_model)
    path_results = 'prediction_results_ABILENE'
    RNNModel.test(path_model, path_results)

if data == 7:

    RNNModel = PredictionClass.RNNPredictionModel(
        n_neurons_per_layer = [100,10,1], # number of neurons per layer in case of multi hidden layer NN
        num_neurons = 100, # number of neurons in case of single hidden layer NN
        num_inputs = 1, # number of inputs 
        num_outputs = 1, # number of outputs in case of single hidden layer NN    
        learning_rate=0.001, # learning rate
        p_train=70, # percentage of training set
        p_validation=20, # percentage of validation set
        p_test=10, # percentage of testing set
        num_train_iterations=5,
        normalization=1, # 1 normalize, 0 otherwise
        activation_function=tf.nn.relu, # activation function: relu, elu
        type_cell='SingleRNN', # type of cell: SingleGRU, SingleLSTM, SingleRNN, MultiGRU, MultiLSTM, MultiRNN
        activate_dropout=1, # Dropout activate = 1, 0 otherwise
        activate_EAM=0, # activate EAM module = 1, 0 otherwise
        thr_iteration=100 # Number of iterations when EAM is activated
        )

    path = 'Data/X_brian.npy'
    couple_nodes = 10
    RNNModel.data_processing_CNN(path)
    path_model = 'prediction_model_BRIAN_X'
    RNNModel.train_CNN(path_model)