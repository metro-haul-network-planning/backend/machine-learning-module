import subprocess
import os
import sys
import PredictionClass
import tensorflow as tf

PATH=os.getcwd()

if(len(sys.argv))>=5:
	path = sys.argv[1]
	n_neurons=int(sys.argv[2])
	t_cell=sys.argv[3]
	couple_nodes = int(sys.argv[4])

else:
	print ("USAGE: {} INPUT OUTPUT ".format(sys.argv[0]))
	sys.exit()


# n_neurons = 10
# t_cell = 'SingleRNN'

RNNModel = PredictionClass.RNNPredictionModel(
n_neurons_per_layer = [100,10,1], # number of neurons per layer in case of multi hidden layer NN
num_neurons = n_neurons, # number of neurons in case of single hidden layer NN
num_inputs = 1, # number of inputs 
num_outputs = 1, # number of outputs in case of single hidden layer NN    
learning_rate=0.001, # learning rate
p_train=70, # percentage of training set
p_validation=20, # percentage of validation set
p_test=10, # percentage of testing set
num_train_iterations=500,
normalization=1, # 1 normalize, 0 otherwise
activation_function=tf.nn.relu, # activation function: relu, elu
type_cell=t_cell, # type of cell: SingleGRU, SingleLSTM, SingleRNN, MultiGRU, MultiLSTM, MultiRNN
activate_dropout=1, # Dropout activate = 1, 0 otherwise
activate_EAM=1, # activate EAM module = 1, 0 otherwise
thr_iteration=100 # Number of iterations when EAM is activated
)


RNNModel.data_processing_6(path, couple_nodes)
path_model = 'prediction_model_ABILENE_'+str(n_neurons)+'_'+t_cell
# RNNModel.train(path_model)
path_results = 'prediction_results_BRIAN'+str(n_neurons)+'_'+t_cell
RNNModel.test(path_model, path_results)

