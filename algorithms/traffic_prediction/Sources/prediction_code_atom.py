import tensorflow as tf
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

# Functions

def next_batch_tm(training_data,batch_size,steps,shift):

    # Grab a random starting point for each batch
    rand_start = np.random.randint(0,len(training_data)-(steps+shift))

    # Create Y data for time series in the batches
    y_batch = np.array(training_data[rand_start:rand_start+steps+shift]).reshape(1,steps+shift)

    # return y_batch[:, :-shift].reshape(-shift, steps, 1), y_batch[:, shift:].reshape(-shift, steps, 1)
    return y_batch[:, :-shift].reshape(-1, steps, 1), y_batch[:, shift:].reshape(-1, steps, 1)

path = 'data/abilene_traffic_matrices.csv'
# path = 'data/total_requests_hourly.csv'
def data_input_tm(path):

    # Data input
    print('Data input')
    data_1 = pd.read_csv(path)
    data_1.index = data_1.timestamp
    data_1 = pd.DataFrame(data_1.value)
    data = data_1
    print('----------------------')
    print('Data samples:')
    print(data.head())
    print('----------------------')

    # data_one_node = np.zeros((len(data)/132))
    # ind = 0
    # for i in range(0,len(data),132):
    #     print i
    #     data_one_node[ind] = data[i]
    #     ind = ind + 1
    # print data_one_node

    return data

def data_processing_tm(data, p_train, p_validation, p_test, tm_vect_dim):

    # Number of train, validation, test samples
    len_data = len(data)/tm_vect_dim
    n_train = int(len_data*p_train/100)+1   #2246
    len_data_2 = len_data-n_train
    n_validation = int(len_data_2*p_validation/100)+1   #561
    n_test = len_data_2-n_validation #401
    tot = n_train+n_validation+n_test
    print '----------------------'
    print  'Total number of samples: ', len_data
    print 'Samples for training: ', n_train, ', Samples for validation:', n_validation, ', Samples for testing the model:', n_test, ',Total:', tot
    print 'Traffic Matrix dimension: ', tm_vect_dim
    print '----------------------'

    train_set = data.iloc[0:n_train*tm_vect_dim]
    validation_set = data.iloc[n_train*tm_vect_dim:(n_train+n_validation)*tm_vect_dim]
    test_set = data.iloc[(n_train+n_validation)*tm_vect_dim:(n_train+n_validation+n_test)*tm_vect_dim]

    # Scale the data
    scaler = MinMaxScaler()
    train_scaled = scaler.fit_transform(train_set)
    validation_scaled = scaler.transform(validation_set)
    test_scaled = scaler.transform(test_set)

    return scaler, train_scaled, validation_scaled, test_scaled, train_set, validation_set, test_set, n_test

def training_test_model(path_model, path_results, scaler, train_scaled, validation_scaled, test_scaled, n_test):

    # Setting Up The RNN Model
    print '----------------------'
    print('Setting parameters')
    # Just one feature, the time series
    num_inputs = 1
    # Num of steps in each batch
    num_time_steps = 6*132
    # 100 neuron layer, play with this
    num_neurons = 100
    # Just one output, predicted time series
    num_outputs = 1
    ## You can also try increasing iterations, but decreasing learning rate
    # learning rate you can play with this
    learning_rate = 0.0001
    # how many iterations to go through (training steps), you can play with this
    num_train_iterations = 1000
    # Size of the batch of data
    batch_size = 1

    X = tf.placeholder(tf.float32, [None, num_time_steps, num_inputs])
    y = tf.placeholder(tf.float32, [None, num_time_steps, num_outputs])

    # Also play around with GRUCell
    cell = tf.contrib.rnn.OutputProjectionWrapper(
        tf.contrib.rnn.BasicLSTMCell(num_units=num_neurons, activation=tf.nn.relu),
        output_size=num_outputs)
    outputs, states = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
    loss = tf.reduce_mean(tf.square(outputs - y)) # MSE
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train = optimizer.minimize(loss)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    # TRAINING
    print('Training')
    MSE = []
    with tf.Session() as sess:
        sess.run(init)
        shift = 1 # 132
        for iteration in range(num_train_iterations):
            X_batch, y_batch = next_batch_tm(train_scaled,batch_size,num_time_steps,shift)
            sess.run(train, feed_dict={X: X_batch, y: y_batch})
            mse = loss.eval(feed_dict={X: X_batch, y: y_batch})
            MSE.append(mse)
            if iteration % 100 == 0:
                print(iteration, "\tMSE:", mse)

        # Save Model for Later
        saver.save(sess, path_model)

    # TEST
    print('Test')
    t_set = list(train_scaled[-num_time_steps:]) + list(test_scaled)
    index_left = 0
    index_right = num_time_steps
    y_pred = []
    with tf.Session() as sess:
        # Use your Saver instance to restore your saved rnn time series model
        shift = 132
        saver.restore(sess, path_model)
        for i in range(n_test): #10
            X_batch = np.array(t_set[index_left+(i*shift):index_right+(i*shift)]).reshape(1, num_time_steps, 1)
            y_pred_TEMP = sess.run(outputs, feed_dict={X: X_batch})
            if i == 0:
                y_pred_2 = y_pred_TEMP[:, -shift:]
            else:
                y_pred_2 = np.concatenate((y_pred_2, y_pred_TEMP[:, -shift:]), axis = 1)

    results = scaler.inverse_transform(np.array(y_pred_2).reshape(test_scaled.size ,1))
    test_set['Generated'] = results
    test_set.to_csv(path_results)



path = 'Data/1_Abilene_Network/abilene_traffic_matrices.csv'
# path = 'data/total_requests_hourly.csv'
data = data_input_tm(path)
[scaler, train_scaled, validation_scaled, test_scaled, train_set, validation_set, test_set, n_test] = data_processing_tm(data, p_train=60, p_validation=30, p_test=10, tm_vect_dim=132)
path_model = "./prediction_model/time_series_model"
path_results = 'Results/predizioni_test_dataframe_shift1.csv'
training_test_model(path_model, path_results, scaler, train_scaled, validation_scaled, test_scaled, n_test)
