#!/usr/bin/python
# ----------------------------------------------------------------------
# offline routing function, by solving ILP model to get the optimal RWA solution in each hour (VWP condition)
# 
# To run the file, it requiresfollowing files:
#  			paths.py
# 			k_disjoint_paths.py
#			"lib" folder
# 
# ----------------------------------------------------------------------
# input: 
# 		    network_after_dimensioning 				
# 			position_file.pkl
# 			color_list_final.pkl
# 			offline_hourly_demand_list.pkl
# output:
#			routing_links_VWP.pkl 						links along each active path
#			open_links_VWP.pkl 							links which are active in each hour
# 			ILP_results_VWP.pkl 						ILP results in each hour, route and wavelengths for each request

from __future__ import division
import sys
import cplex
import copy
import random
import math
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pickle
from random import Random, uniform
from paths import *
import k_disjoint_paths as k_paths

import os
from os.path import expanduser
home = expanduser("~")
print home
sys.path.append(home+'/git-projects/metro-haul-network-planning/backend/machine-learning-module/')
from params import *
	

def create_one_time_demand(demand_list, time_point):
	'''
	create demands at a certain time time_point
	'''
	one_time_demand = copy.deepcopy(demand_list)
	for i in range(len(one_time_demand)):
		one_time_demand[i]['amount'] = demand_list[i]['amount'][time_point]

	return one_time_demand


def simple_disjoint_path(graph_origin, path, backup_list, src, dst):
	'''
	generate a pair of disjoint paths 
	'''
	graph = copy.deepcopy(graph_origin)
	path_links = [(path[n],  path[n + 1]) for n in range(len(path) - 1)]
	graph.remove_edges_from(path_links)
	n = 0
	workpath = None
	backuppath = None
	if not nx.has_path(graph, src, dst):
		return None, None
	disjoint_paths = list(nx.all_simple_paths(graph, src, dst))
	disjoint_paths = sorted(disjoint_paths, key = len)
	while backuppath == None or backuppath in backup_list:
		disjoint_path = disjoint_paths[n]
		n += 1
		if len(path) <= len(disjoint_path):
			workpath = path
			backuppath = disjoint_path
		else:
			backuppath = path
			workpath = disjoint_path
	return workpath, backuppath
		
	
def paths_gen(graph_origin, src_w, src_b, dst_w, dst_b):
	'''
	generate disjoint path pairs
	'''
	graph = copy.deepcopy(graph_origin)
	# generate vitual links and nodes source and dstination
	if src_w == src_b:
		src = src_w
	else:
		src = "src"
		graph.add_edge(src, src_w)
		graph.add_edge(src, src_b)
	if dst_w == dst_b:
		dst = dst_w
	else:
		dst = "dst"
		graph.add_edge(dst_w, dst)
		graph.add_edge(dst_b, dst)

	path_pair = []
	for (u, v) in graph.edges():
		graph[u][v]['weight'] = 1

	length = nx.shortest_path_length(graph, src, dst)

	shortest_path = list(nx.all_simple_paths(graph, src, dst))
	shortest_path = sorted(shortest_path, key = len)


	backup = []
	n = 0
	
	stop=1
	for path_0 in shortest_path:
		#path_0 = shortest_path[n]
		print path_pair
		n += 1
		path_1, path_2 = simple_disjoint_path(graph, path_0, backup, src, dst)
		if path_1 == None or path_2 == None:
			break
		path_pair.append({})
		if src == "src":
			path_1 = path_1[1:]
			path_2 = path_2[1:]
		if dst == "dst":
			path_1 = path_1[:-1]
			path_2 = path_2[:-1]
		path_pair[-1]["workingpath"] = path_1
		path_pair[-1]["backuppath"] = path_2
		backup.append(path_pair[-1]["backuppath"])
		#print len(path_pair),len(shortest_path)
	
	
	
	
	
	
	
	#while len(path_pair) != 4:
	#	path_0 = shortest_path[n]
	#	n += 1
	#	path_1, path_2 = simple_disjoint_path(graph, path_0, backup, src, dst)
	#	if path_1 == None or path_2 == None:
	#		continue
	#	path_pair.append({})
	#	if src == "src":
	#		path_1 = path_1[1:]
	#		path_2 = path_2[1:]
	#	if dst == "dst":
	#		path_1 = path_1[:-1]
	#		path_2 = path_2[:-1]
	#	path_pair[-1]["workingpath"] = path_1
	#	path_pair[-1]["backuppath"] = path_2
	#	backup.append(path_pair[-1]["backuppath"])
		#print len(path_pair),len(shortest_path)

	return path_pair



def offline_ILP(graph_origin, demand_list, wav_per_fiber, ALPHA, GAMMA, BETA):
	graph = copy.deepcopy(graph_origin)
	for item in demand_list:
		path_pairs = paths_gen(graph, item['src_w'], item['src_b'], item['dst_w'], item['dst_b'])
		item["paths"] = path_pairs

	c = cplex.Cplex()
	c.objective.set_sense(c.objective.sense.minimize)

	var_names = (['x' + str(i) + ',' + str(l) + ',' + str(j)   # x(i,l,j)  lth request of demand d 
                                                                              # routed on  path pair p
                for i in range(len(demand_list))
                for l in range(int(demand_list[i]["amount"])) 
                for j in range(len(demand_list[i]['paths']))]
                +
                ['fiber' + i + ',' + j for (i, j) in graph.edges()]
                +
                ["w" + i + ',' + j for (i, j) in graph.edges()]) # f(e) active fiber number on link e

	# power consumption
	my_obj = []
	count_x = 0
	for i in range(len(demand_list)):
		for l in range(int(demand_list[i]["amount"])):
			for j in range(len(demand_list[i]['paths'])):
				cost_x = 0
				count_x += 1
				my_obj.append(cost_x)
	for (i, j) in graph.edges():
		cost_fiber = ALPHA + GAMMA
		my_obj.append(cost_fiber)
	for (i, j) in graph.edges():
		cost_wav = BETA
		my_obj.append(cost_wav)



	c.variables.add(obj   = my_obj,
                    types = "B" * count_x + "I" * 2 * graph.number_of_edges(),
                    names = var_names)


    # solenoidality for x 
	for i in range(len(demand_list)):
		for l in range(int(demand_list[i]["amount"])):
			c.linear_constraints.add(lin_expr = [[['x' + str(i) + ',' + str(l) + ',' + str(j)
	                                                for j in range(len(demand_list[i]['paths'])) ], 
	                                                [1 for j in range(len(demand_list[i]['paths']))]]],
	                                senses = ["E"],
	                                rhs = [1])

    # capacity
	for (p, q) in graph.edges():

		path_pair_dic = {}
		for i in range(len(demand_list)):
			path_pair_dic[i] = []
			for j in range(len(demand_list[i]["paths"])):
				link = [(demand_list[i]["paths"][j]["workingpath"][nn], 
			                demand_list[i]["paths"][j]["workingpath"][nn + 1])
			                for nn in range(len(demand_list[i]["paths"][j]["workingpath"]) - 1)]
				link += [(demand_list[i]["paths"][j]["backuppath"][nn], 
			                demand_list[i]["paths"][j]["backuppath"][nn + 1])
			                for nn in range(len(demand_list[i]["paths"][j]["backuppath"]) - 1)]
				if (p, q) in link:
					path_pair_dic[i].append(j)

		c.linear_constraints.add(lin_expr = [[['x' + str(i) + ',' + str(l) + ',' + str(j) 
                                            for i in range(len(demand_list))
                                            for l in range(int(demand_list[i]["amount"]))
                                            for j in path_pair_dic[i]]
                                            + 
                                            ['fiber' + p + ',' + q], 
                                            [1 for i in range(len(demand_list))
                                            for l in range(int(demand_list[i]["amount"]))
                                            for j in path_pair_dic[i]]
                                            +
                                            [-wav_per_fiber]]],
                            		senses = ["L"],
                            		rhs = [0])

		c.linear_constraints.add(lin_expr = [[['fiber' + p + ',' + q], [1]]],
                                		senses = ["L"],
                                		rhs = [round(graph[p][q]["fiber"])])
		# calculate active wav number on link e
		c.linear_constraints.add(lin_expr = [[['x' + str(i) + ',' + str(l) + ',' + str(j)  
                                                for i in range(len(demand_list))
                                                for l in range(int(demand_list[i]["amount"]))
                                                for j in path_pair_dic[i]]
                                                + 
                                                ['w' + p + ',' + q], 
                                                [1 for i in range(len(demand_list))
                                                for l in range(int(demand_list[i]["amount"]))
                                                for j in path_pair_dic[i]]
                                                +
                                                [-1]]],
                                		senses = ["E"],
                                		rhs = [0])
		# guarantee free channels
		c.linear_constraints.add(lin_expr = [[['w' + p + ',' + q], 
                                                [1]]],
                                		senses = ["L"],
                                		rhs = [wav_per_fiber * 0.9 * round(graph[p][q]["fiber"])])

	# for p,q in graph.edge():
	# 	print graph[p][q]["fiber"]
		
	c.set_log_stream(None)
	c.set_error_stream(None)
	c.set_warning_stream(None)
	c.set_results_stream(None)
	
	c.solve()

	link_wav_fiber_assignment = {}
	for (p, q) in graph.edges():
		link_wav_fiber_assignment[p + ',' + q] = 0


	results = {}
	routing_links = {}
	for i in range(len(demand_list)):
		# demand i
		routing_links[i] = {}
		for l in range(int(demand_list[i]["amount"])):
			# request l
			routing_links[i][l] = {}
			for j in range(len(demand_list[i]['paths'])):
				if round(c.solution.get_values('x' + str(i) + ',' + str(l) + ',' + str(j))) == 1:					
					routing_links[i][l]["worklinks"] = []
					w = demand_list[i]["paths"][j]["workingpath"]
					for n in range(len(w) - 1):
						fiber_num = link_wav_fiber_assignment[w[n] + ',' + w[n + 1]] // wav_per_fiber
						wav_num = link_wav_fiber_assignment[w[n] + ',' + w[n + 1]] % wav_per_fiber
						routing_links[i][l]["worklinks"].append((w[n] + "_" + str(int(wav_num)) + "_" + str(int(fiber_num)),
							                                    w[n + 1] + "_" + str(int(wav_num)) + "_" + str(int(fiber_num))))
						link_wav_fiber_assignment[w[n] + ',' + w[n + 1]] += 1
						# calculate No of wavelegnths on the links
						if (w[n], w[n + 1]) in results.keys():
							results[(w[n], w[n + 1])]['wavelengths'] += 1
						else:
							results[(w[n], w[n + 1])] = {}
							results[(w[n], w[n + 1])]['wavelengths'] = 1

					routing_links[i][l]["backuplinks"] = []
					b = demand_list[i]["paths"][j]["backuppath"]
					for n in range(len(b) - 1):
						fiber_num = link_wav_fiber_assignment[b[n] + ',' + b[n + 1]] // wav_per_fiber
						wav_num = link_wav_fiber_assignment[b[n] + ',' + b[n + 1]] % wav_per_fiber
						routing_links[i][l]["backuplinks"].append((b[n] + "_" + str(int(wav_num)) + "_" + str(int(fiber_num)),
							                                    b[n + 1] + "_" + str(int(wav_num)) + "_" + str(int(fiber_num))))
						link_wav_fiber_assignment[b[n] + ',' + b[n + 1]] += 1
						# calculate No of wavelegnths on the links
						if (b[n], b[n + 1]) in results.keys():
							results[(b[n], b[n + 1])]['wavelengths'] += 1
						else:
							results[(b[n], b[n + 1])] = {}
							results[(b[n], b[n + 1])]['wavelengths'] = 1
			if routing_links[i][l] == {}:
				print "error"
	open_link = []
	for (p, q) in graph.edges():
		#print "fiber" + p + "," + q, c.solution.get_values("fiber" + p + "," + q)
		#print graph[p][q]["fiber"]
		if round(c.solution.get_values("fiber" + p + "," + q)) > 0:
			open_link.append((p, q))

			if (p, q) not in results.keys():
				print "results are wrong!!!!!"
			results[(p, q)]['fibers'] = round(c.solution.get_values("fiber" + p + "," + q))


	return routing_links, open_link, results     



def main():

	if not os.path.exists(PATH_FOLDER_OFFLINE_DEMAND_RESULTS):
		os.makedirs(PATH_FOLDER_OFFLINE_DEMAND_RESULTS)

	G = nx.DiGraph()
	G = nx.read_edgelist(PATH_FOLDER_GENERATED_DATA+'network_after_dimensioning', 
							nodetype = str, create_using = nx.DiGraph())
	pos = pickle.load(open(PATH_FOLDER_GENERATED_DATA+'position_file.pkl', 'rb'))
	color_list = pickle.load(open(PATH_FOLDER_GENERATED_DATA+'color_list_final.pkl','rb'))

	
	for day in range(1,NUMBER_OFFLINE_DAYS+1):
		print day
		name=PATH_FOLDER_OFFLINE_DEMAND_DATA+'offline_hourly_demand_list_'+str(day)+'.pkl'
		average_hourly_demand_list = pickle.load(open(name, 'rb'))

		links_info = {}
		open_link = {}
		result = {}

		for i in range(24):
			print i
			one_time_demand = create_one_time_demand(average_hourly_demand_list, i)
			routing_links, open_links, results = offline_ILP(G, one_time_demand, NUM_OF_WAV, ALPHA_SUM, GAMMA, BETA)
			links_info[i] = routing_links
			open_link[i] = open_links
			result[i] = results

		name=PATH_FOLDER_OFFLINE_DEMAND_RESULTS+'routing_links_VWP_'+str(day)+'.pkl'
		pickle.dump(links_info, open(name, 'wb'))
		name=PATH_FOLDER_OFFLINE_DEMAND_RESULTS+'open_links_VWP_'+str(day)+'.pkl'
		pickle.dump(open_link, open(name, 'wb'))
		name=PATH_FOLDER_OFFLINE_DEMAND_RESULTS+'ILP_results_VWP_'+str(day)+'.pkl'
		pickle.dump(result, open(name, 'wb'))
	
	


if __name__ == '__main__':

	main()