#!/usr/bin/python
# ----------------------------------------------------------------------
# online routing function using Discrete event simulator (WP condition)
# RWA is done every hour
# 
# To run the file, it requiresfollowing files:
#  			paths.py
#			"lib" folder
# 
# ----------------------------------------------------------------------
# input: 
# 		    network_after_dimensioning 				
# 			position_file.pkl
# 			color_list_final.pkl
# 			hourly_demand_list.pkl
#           routing_links_WP.pkl
# 			open_links_WP.pkl
# output:
#			OnlineRWA_results_WP.pkl 						open links and wavelengths information in each hour
#			OnlineRWA_results_total_WP.pkl' 				sum od open links and wavelengths info from all the hours
# 			OnlineRWA_day_results_WP.pkl 					daily open links and wavelengths info 



from __future__ import division
import sys
import copy
import random
import math
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pickle
from random import Random, uniform, expovariate, uniform
from paths import *
from SimPy.Simulation import *

class Links:
	routing_links = pickle.load(open('../../generated_dataset/offline_results/routing_links_WP_47.pkl', 'rb'))
	open_links = pickle.load(open('../../generated_dataset/offline_results/open_links_WP_47.pkl', 'rb'))

class G(Process):
	Rnd = Random(12345)
	CurrentHour = 0
	DemandProcessList = {}
	NetworkProcess = None

	Graph = nx.read_edgelist('../../generated_dataset/network_after_dimensioning', 
								nodetype = str, create_using = nx.DiGraph())
	pos = pickle.load(open('../../generated_dataset/position_file.pkl', 'rb'))
	color_list = pickle.load(open('../../generated_dataset/color_list_final.pkl','rb'))

	#plt.show()

	NUM_OF_WAV = 80
	NUM_OF_FIBER = 4
	STOP_RATIO = 0.1

	hourly_demand_list = pickle.load(open('../../generated_dataset/hourly_demand_list.pkl', 'rb'))
	Reallocation = [{} for i in range(len(hourly_demand_list))]

	network_origin = None
	demand_route_table_origin = None
	link_states_table_origin = None

	result = {}
	result_2 = {}
	day_result = {}


	def __init__(self):
		Process.__init__(self)

	def Run(self):
		convert_demand_list(G.hourly_demand_list)
		pickle.dump(G.hourly_demand_list, open('../../generated_dataset/hourly_demand_state.pkl', 'wb'))
		#nx.draw_networkx(G.Graph, G.pos, with_labels = False, 
					#node_size = 50, node_color = G.color_list)
		# prepare tables for recording network states
		G.network_origin = convert_graph(G.Graph, G.NUM_OF_WAV)
			
		G.demand_route_table_origin = create_demand_route_table(G.hourly_demand_list)
		G.link_states_table_origin = create_link_state_table(G.network_origin, G.NUM_OF_FIBER, G.NUM_OF_WAV)
		yield passivate, self


class DemandProcess(Process):
	def __init__(self,ID):
		Process.__init__(self)
		self.ID = ID
		G.DemandProcessList[self.ID] = self
	def Run(self):
		while 1:
			G.CurrentHour = int(now() // 3600)
			reallocation_list = copy.deepcopy(G.hourly_demand_list[self.ID])
			reallocation_list['amount'] = reallocation_list['amount'][G.CurrentHour]
			G.Reallocation[self.ID] = reallocation_list
			reactivate(G.NetworkProcess) 
			yield hold, self, 3600

class IrregularProcess(Process):
	IrregularRate = 1.0/14400
	def __init__(self,ID):
		Process.__init__(self)
		self.ID = ID
	def Run(self):
		while 1:
			None

class Network(Process):
	def __init__(self):
		Process.__init__(self)
		G.NetworkProcess = self
	def Run(self):
		while 1:
			yield passivate, self
			print "reallcoaction list", G.Reallocation
			print "time", now()

			network = copy.deepcopy(G.network_origin)
			# generated every hour
			wav_networks_origin = create_wav_networks(G.network_origin, G.NUM_OF_WAV)

			wav_networks = copy.deepcopy(wav_networks_origin)
			demand_route_table = copy.deepcopy(G.demand_route_table_origin)
			link_states_table = copy.deepcopy(G.link_states_table_origin)

			one_time_demand = G.Reallocation

			hourly_routing_wav_assignment(network, one_time_demand, 
				                          wav_networks, wav_networks_origin, 
				                          demand_route_table, link_states_table)

			G.Reallocation = [{} for i in range(len(G.hourly_demand_list))]
			
			
			for i in range(G.NUM_OF_WAV):
				for (p, q) in network.edges_iter():
					for j in range(G.NUM_OF_FIBER):
						index = link_states_table['link' + p + ',' + q][i][j]
						if index != None:
							exist = False
							for item in demand_route_table[index]:
								w = item['workingpath']
								b = item['backuppath']
								for k in range(len(w) - 1):
									if (w[k], w[k+1]) ==(p,q) and item["wav_seq_w"][k] == i\
															  and item["fiber_seq_w"][k] == j:
										exist = True
								for k in range(len(b) - 1):
									if (b[k], b[k+1]) ==(p,q) and item["wav_seq_b"][k] == i\
															  and item["fiber_seq_b"][k] == j:
										exist = True 
							if not exist:
								print "error tables"

			G.result[int(now()/3600)] = {}
			for (p, q) in G.network_origin.edges_iter():
				list_fiber = []
				for i in range(G.NUM_OF_WAV):
					for j in range(G.NUM_OF_FIBER):
						if link_states_table['link' + p +',' + q][i][j] != None:
							if j not in list_fiber:
								list_fiber.append(j)
							if (p, q) in G.result[int(now()/3600)].keys():
								G.result[int(now()/3600)][(p, q)]['wavelengths'] += 1
							else:
								G.result[int(now()/3600)][(p, q)] = {}
								G.result[int(now()/3600)][(p, q)]['wavelengths'] = 1

				if list_fiber != []:
					if (p, q) not in G.result[int(now()/3600)].keys():
						print "results are wrong!!!!!!"
					else:
						G.result[int(now()/3600)][(p, q)]['fibers'] = len(list_fiber)

			# process the result
			G.result_2[int(now()/3600)] = {}
			tot_hour_now = int(now()/3600)
			hour_now = tot_hour_now % 24
			day_now = tot_hour_now // 24
			G.result_2[tot_hour_now]['total_wav'] = 0
			G.result_2[tot_hour_now]['total_fiber'] = 0
			for key in G.result[int(now()/3600)].keys():
				G.result_2[tot_hour_now]['total_wav'] += G.result[int(now()/3600)][key]['wavelengths']
				G.result_2[tot_hour_now]['total_fiber'] += G.result[int(now()/3600)][key]['fibers']

			if hour_now == 23:
				G.day_result[day_now] = {}
				G.day_result[day_now]['average_sequence'] = []
				G.day_result[day_now]['error_rate_sequence'] = []
				for i in range(24):
					G.day_result[day_now]['average_sequence'].append(np.mean([G.result_2[24 * j + i]['total_wav'] 
																			for j in range(0, day_now + 1)]))
					G.day_result[day_now]['error_rate_sequence'].append(np.mean([abs(G.result_2[24 * j + i]['total_wav'] 
																			    - 
																			    G.day_result[day_now]['average_sequence'][i])
																			    /
																			    G.day_result[day_now]['average_sequence'][i]
																			for j in range(0, day_now + 1)]))
				
				pickle.dump(G.result_2, open('../../generated_dataset/online_results/OnlineRWA_results_total_WP_47.pkl', 'wb'))
				pickle.dump(G.day_result, open('../../generated_dataset/online_results/OnlineRWA_day_results_WP_47.pkl', 'wb'))

				if day_now > 30:
					stopSimulation()


def physical_node(node):
	for n in range(len(node)):
		if node[n] == "_":
			node_name = copy.deepcopy(node[:n])
			break
	return node_name


def add_weights(graph, d, r, t):
	SMALL = 0.0001
	ACTIVE_COST = 65 / 50
	t = int(t)
	if r <= len(Links.routing_links[t][d]) - 1:
		links_w = Links.routing_links[t][d][r]["worklinks"]
		links_b = Links.routing_links[t][d][r]["backuplinks"]
		other_assigned_links = []
		for i in range(len(Links.routing_links[t])):
			for j in range(len(Links.routing_links[t][i])):
				if i != d and j != r:
					other_assigned_links += Links.routing_links[t][i][j]["worklinks"]
					other_assigned_links += Links.routing_links[t][i][j]["backuplinks"]
	else:
		other_assigned_links = []
		for i in range(len(Links.routing_links[t])):
			for j in range(len(Links.routing_links[t][i])):
				other_assigned_links += Links.routing_links[t][i][j]["worklinks"]
				other_assigned_links += Links.routing_links[t][i][j]["backuplinks"]

	for (p, q) in graph.edges_iter():
		if check_same_node(p, q):
			graph[p][q]["weight"] = 0
		elif r <= len(Links.routing_links[t][d]) - 1:
			if (p, q) in links_w:
				graph[p][q]["weight"] = SMALL
			elif (p, q) in links_b:
				graph[p][q]["weight"] = 1
			elif (p, q) in other_assigned_links:
				graph[p][q]["weight"] = float("Inf")
			elif (physical_node(p), physical_node(q)) not in Links.open_links[t]:
				graph[p][q]["weight"] = len(links_b) + 1
			elif (physical_node(p), physical_node(q)) in Links.open_links[t]:
				graph[p][q]["weight"] = (len(links_b) + 1) * (1 + ACTIVE_COST)
		else:
			if (p, q) in other_assigned_links:
				graph[p][q]["weight"] = float("Inf")
			elif (p, q) not in Links.open_links[t]:
				graph[p][q]["weight"] = SMALL
			elif (p, q) in Links.open_links[t]:
				graph[p][q]["weight"] = SMALL * (1 + ACTIVE_COST)



def convert_graph(graph, num_wav):
	'''
	remove 'fiber' attribute of each link,
	add wavelength attribute on each link
	'''
	graph_copy = copy.deepcopy(graph)
	for (p, q) in graph_copy.edges_iter():
		for i in range(num_wav):
			graph_copy[p][q]['wav' + str(int(i))] = graph_copy[p][q]['fiber']

	return graph_copy


def convert_demand_list(demand_list):
	'''
	initially the value in demand list is the wavelength number difference between
	current time point and previous time point, instead of storing the difference,
	now need store real wavelength number at current time point, so it requires a 
	convertion
	'''
	for item in demand_list:
		for i in range(len(item['amount'])):
			if i == 0:
				continue
			else:
				item['amount'][i] += item['amount'][i - 1]


def check_same_node(node_1, node_2):
	""""
	to check if the two nodes of the layered graph are in the same physical node
	"""
	for n in range(len(node_1)):
		if node_1[n] == "_":
			node_name_1 = node_1[:n]
			break
	for n in range(len(node_2)):
		if node_2[n] == "_":
			node_name_2 = node_2[:n]
			break
	if node_name_1 == node_name_2:
		return True
	else:
		return False 


def create_wav_networks(graph, num_wav):
	'''
	according to original graph, create an available network 
	for all wavelengths, create dummy node for each node, add
	capacity to each node, no fiber switch and no wav converter.
	'''
	wav_network = nx.DiGraph()
	for i in range(num_wav):
		for (p, q) in graph.edges_iter():
			for j in range(int(graph[p][q]['wav' + str(int(i))])):
				a = p + "_" + str(int(i)) + "_" + str(int(j))
				b = q + "_" + str(int(i)) + "_" + str(int(j))
				wav_network.add_edge(a, b)
				wav_network[a][b]['num'] = 1

				if not wav_network.has_edge(a, p + "_d"):
					wav_network.add_edge(a, p + "_d", num = float("inf"))
				if not wav_network.has_edge(p + "_s", a):
					wav_network.add_edge(p + "_s", a, num = float("inf"))

				if not wav_network.has_edge(b, q + "_d"):
					wav_network.add_edge(b, q + "_d", num = float("inf"))
				if not wav_network.has_edge(q + "_s", b):
					wav_network.add_edge(q + "_s", b, num = float("inf"))

	for i in range(num_wav):
		for node in graph.nodes_iter():
			same_node_list = []
			for vertex in wav_network.nodes_iter():
				if vertex[:len(node + "_" + str(int(i)))] == node + "_" + str(int(i)):
					same_node_list.append(vertex)
			for n in range(len(same_node_list) - 1):
				wav_network.add_edge(same_node_list[n], same_node_list[n + 1])
				wav_network.add_edge(same_node_list[n + 1], same_node_list[n])

	return wav_network


def create_demand_route_table(demand_list):
	'''
	create an empty demand route table,
	for each demand, there are three keys:['workingpath'],['backuppath'],['wav'],['num']
	'''
	demand_route_table = {}
	for i in range(len(demand_list)):
		demand_route_table[i] = []

	return demand_route_table


def create_link_state_table(graph, num_fiber, num_wav):
	'''
	create an empty table recording link states,
	list[link name][wav num][fiber num] = demand num_wav
	'''
	link_states_table = {}
	for (p, q) in graph.edges_iter():
		link_states_table['link' + p +',' + q] = {}
		for i in range(num_wav):
			link_states_table['link' + p +',' + q][i] = {}
			for j in range(num_fiber):
				link_states_table['link' + p +',' + q][i][j] = None

	return link_states_table


def create_one_time_demand(demand_list, time_point):
	'''
	create demands at a certain time time_point
	'''
	one_time_demand = copy.deepcopy(demand_list)
	for i in range(len(one_time_demand)):
		one_time_demand[i]['amount'] = demand_list[i]['amount'][time_point]

	return one_time_demand


def Bhandari_disjoint_paths(graph_origin, src_w, src_b, dst_w, dst_b):
	graph = copy.deepcopy(graph_origin)
	#workpath = nx.dijkstra_path(graph, src_w, dst_w)

	pred, dist = nx.bellman_ford(graph, src_w)
	workpath = [dst_w]
	if dst_w not in pred.keys():
		return [], [], float('inf'), float('inf')
	while workpath[0] != src_w:
		for key in pred.keys():
			if key == workpath[0]:
				workpath.insert(0, pred[key])
				break

	for i in range(len(workpath) - 1):
		# left node of the selected link
		for n_1 in range(len(workpath[i])):
			if workpath[i][n_1] == "_":
				node_1 = workpath[i][:n_1]
				break
		# right node of the selected link
		for n_2 in range(len(workpath[i + 1])):
			if workpath[i + 1][n_2] == "_":
				node_2 = workpath[i + 1][:n_2]
				break
		# find the selected link in all layers and put the weight infinite
		if node_1 != node_2:
			for (p, q) in graph_origin.edges_iter():
				if p[:n_1] == node_1 and q[:n_2] == node_2:
					graph.remove_edge(p, q)
				if p[:n_1] == node_2 and q[:n_2] == node_1:
					graph.remove_edge(p, q)
			"""
			# set the weights of reverse links along the workpath negative  
			if graph.has_edge(workpath[i + 1], workpath[i]):
				graph[workpath[i + 1]][workpath[i]]['weight'] = \
					-graph_origin[workpath[i]][workpath[i + 1]]['weight']
			else:
				graph.add_edge(workpath[i + 1], workpath[i], 
					weight = -graph_origin[workpath[i]][workpath[i + 1]]['weight'])
			#graph.remove_edge(workpath[i], workpath[i + 1])
		    """
		if graph.has_edge(workpath[i + 1], workpath[i]):
				graph[workpath[i + 1]][workpath[i]]['weight'] = \
					-graph_origin[workpath[i]][workpath[i + 1]]['weight']
		else:
			graph.add_edge(workpath[i + 1], workpath[i], 
				weight = -graph_origin[workpath[i]][workpath[i + 1]]['weight'])
		#graph.remove_edge(workpath[i], workpath[i + 1])


	pred, dist = nx.bellman_ford(graph, src_b)
	backuppath = [dst_b]
	if dst_b not in pred.keys():
		return [], [], float('inf'), float('inf')
	while backuppath[0] != src_b:
		for key in pred.keys():
			if key == backuppath[0]:
				backuppath.insert(0, pred[key])
				break 
	print workpath
	print backuppath

	graph_new = nx.DiGraph()
	graph_new.add_path(workpath)
	graph_new.add_path(backuppath)
	remove_list = []
	for (i,j) in graph_new.edges_iter():
	    if graph_new.has_edge(j,i) and not check_same_node(i, j):
	        remove_list.append((i,j))
	graph_new.remove_edges_from(remove_list)

	path_w = nx.dijkstra_path(graph_new, src_w, dst_w)
	path_w_length = nx.dijkstra_path_length(graph_new, src_w, dst_w)
	for i in range(1, len(path_w) - 2):
		if not check_same_node(path_w[i], path_w[i + 1]):
			graph_new.remove_edge(path_w[i], path_w[i + 1])
	path_b = nx.dijkstra_path(graph_new, src_b, dst_b)
	path_b_length = nx.dijkstra_path_length(graph_new, src_b, dst_b)

	return path_w, path_b, path_w_length, path_b_length


def hourly_routing_wav_assignment(network, demand_list, wav_network, 
	                              wav_networks_origin, route_table, link_table):
	'''
	perform online routing of traffic demand and record routing data 
	'''
	switch_off_list = []
	for i in range(len(demand_list)):
		if demand_list[i]['amount'] < 0:
			switch_off_list.append(i)

	switch_on_list = []
	for i in range(len(demand_list)):
		if demand_list[i]['amount'] > 0:
			switch_on_list.append(i)

	## switch off demand implementation
	for index in switch_off_list:
		for j in range(int(-demand_list[index]['amount'])):
			# update demand route table
			route_table[index][0]['num'] -= 1
			W = route_table[index][0]['workingpath']
			B = route_table[index][0]['backuppath']
			wav_seq_w = route_table[index][0]['wav_seq_w']
			wav_seq_b = route_table[index][0]['wav_seq_b']
			fiber_seq_w = route_table[index][0]['fiber_seq_w']
			fiber_seq_b = route_table[index][0]['fiber_seq_b']
			del route_table[index][0]

			# update network and wavelength network graph and link state table
			for n in range(len(W) - 1):
				wav_network.add_edge(W[n] + "_" + str(wav_seq_w[n]) + "_" + str(fiber_seq_w[n]),\
				           W[n + 1] + "_" + str(wav_seq_w[n]) + "_" + str(fiber_seq_w[n]), num = 1)

				link_table['link' + W[n] + ',' + W[n + 1]][wav_seq_w[n]][fiber_seq_w[n]] == None

			for n in range(len(B) - 1):
				wav_network.add_edge(B[n] + "_" + str(wav_seq_b[n]) + "_" + str(fiber_seq_b[n]),\
				           B[n + 1] + "_" + str(wav_seq_b[n]) + "_" + str(fiber_seq_b[n]), num = 1)

				link_table['link' + B[n] + ',' + B[n + 1]][wav_seq_b[n]][fiber_seq_b[n]] == None

	## switch on demand implementation
	weights_update_list = {}
	for (p, q) in wav_network.edges_iter():
		weights_update_list[(p, q)] = 0 # occupied links
	for index in switch_on_list:
		# add weights on the graph for each deamdn in each hour
		for j in range(int(demand_list[index]['amount'])):
			# update weights for occupied links
			for (u, v) in weights_update_list.keys():
				if weights_update_list[(u, v)] == 1 and wav_network.has_edge(u, v):
					wav_network.remove_edge(u, v)

			add_weights(wav_network, index, j, now() / 3600 % 24)
			wav_network_copy = copy.deepcopy(wav_network)

			infinite_list = []
			for (u, v) in wav_network_copy.edges_iter():
				if wav_network_copy[u][v]["weight"] == float("Inf"):
					infinite_list.append((u, v))
			wav_network_copy.remove_edges_from(infinite_list)

			print "--------------",0
			path_w, path_b, path_w_length, path_b_length = \
					             Bhandari_disjoint_paths(
					             	wav_network_copy, 
					             	demand_list[index]['src_w'] + "_s", 
					             	demand_list[index]['src_b'] + "_s" , 
					             	demand_list[index]['dst_w'] + "_d", 
					             	demand_list[index]['dst_b'] + "_d")
			print "--------------",1

			if path_w_length >= 1 / sys.float_info.epsilon or \
			   path_b_length	>= 1 / sys.float_info.epsilon:
				print "there is an error finding path pair"

			for n in range(1, len(path_w) - 2):
				if not check_same_node(path_w[n], path_w[n + 1]):
					weights_update_list[(path_w[n], path_w[n + 1])] = 1
			for n in range(1, len(path_b) - 2):
				if not check_same_node(path_b[n], path_b[n + 1]):
					weights_update_list[(path_b[n], path_b[n + 1])] = 1			

			# update tables for nodes along working path
			name_list = []
			wav_list = []
			fiber_list = []
			for n in range(1, len(path_w) - 1):    #do not consider dummy nodes
				space_position = []
				for nn in range(len(path_w[n])):
					if path_w[n][nn] == "_":
						space_position.append(nn)
				node_name = path_w[n][:space_position[0]]
				wav_plane = path_w[n][(space_position[0] + 1):space_position[1]]
				fiber_plane = path_w[n][(space_position[1] + 1):]

				name_list.append(node_name)
				wav_list.append(int(wav_plane))
				fiber_list.append(int(fiber_plane))

			name_w = [name_list[0]]
			wav_w = []
			fiber_w = []
 			for n_1 in range(1,len(name_list)):
				if name_list[n_1 - 1] == name_list[n_1]:
					continue
				else:
					link_table['link' + name_list[n_1 - 1] + ',' + name_list[n_1]][wav_list[n_1]][fiber_list[n_1]] = index
					name_w.append(name_list[n_1])
					wav_w.append(wav_list[n_1])
					fiber_w.append(fiber_list[n_1])

			# update tables for nodes along backup path
			name_list = []
			wav_list = []
			fiber_list = []
			for n in range(1, len(path_b) - 1):    #do not consider dummy nodes
				space_position = []
				for nn in range(len(path_b[n])):
					if path_b[n][nn] == "_":
						space_position.append(nn)
				node_name = path_b[n][:space_position[0]]
				wav_plane = path_b[n][(space_position[0] + 1):space_position[1]]
				fiber_plane = path_b[n][(space_position[1] + 1):]

				name_list.append(node_name)
				wav_list.append(int(wav_plane))
				fiber_list.append(int(fiber_plane))

			name_b = [name_list[0]]
			wav_b = []
			fiber_b = []
			for n_1 in range(1,len(name_list)):
				if name_list[n_1 - 1] == name_list[n_1]:
					continue
				else:
					link_table['link' + name_list[n_1 - 1] + ',' + name_list[n_1]][wav_list[n_1]][fiber_list[n_1]] = index
					name_b.append(name_list[n_1])
					wav_b.append(wav_list[n_1])
					fiber_b.append(fiber_list[n_1])

			# update demand route table
			route_table[index].append({})
			route_table[index][-1]['workingpath'] = name_w
			route_table[index][-1]['backuppath'] = name_b
			route_table[index][-1]['wav_seq_w'] = wav_w
			route_table[index][-1]['wav_seq_b'] = wav_b
			route_table[index][-1]['fiber_seq_w'] = fiber_w
			route_table[index][-1]['fiber_seq_b'] = fiber_b



def main():
	initialize()
	Global = G()
	activate(Global, Global.Run())
	Net = Network()
	activate(Net, Net.Run())
	for i in range(len(G.hourly_demand_list)):
		R = DemandProcess(i)
		activate(R, R.Run())
	hour = len(G.hourly_demand_list[0]['amount'])
	maximum = (hour - 1) * 3600
	simulate(until = maximum)
	pickle.dump(G.result, open('../../generated_dataset/online_results/OnlineRWA_results_WP_47.pkl', 'wb'))
	pickle.dump(G.result_2, open('../../generated_dataset/online_results/OnlineRWA_results_total_WP_47.pkl', 'wb'))
	pickle.dump(G.day_result, open('../../generated_dataset/online_results/OnlineRWA_day_results_WP_47.pkl', 'wb'))


if __name__ == '__main__':
	main()
