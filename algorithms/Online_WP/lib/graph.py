#!/usr/bin/env python
'''
Library functions for working w/graphs that are not specific to an algorithm.
'''

import logging

import networkx as nx

lg = logging.getLogger("cc")


def flatten(paths):
    '''Compute and return flattened graph, given paths.

    By flattened graph, we mean one created from the union of a set of paths.

    @param paths: paths to flatten

    @return flattened: flattened NetworkX Graph
    '''
    used = nx.Graph()
    lg.debug("paths: %s" % paths)
    for path in paths.values():
        lg.debug("flattening path: %s" % path)
        used.add_path(path)
    return used


def loop_graph(n):
    '''Return loop graph with n nodes.'''
    g = nx.path_graph(n)
    # Add loop edge
    g.add_edge(g.number_of_nodes() - 1, 0)
    return g


def set_unit_weights(g):
    '''Set edge weights for NetworkX graph to 1 & return.'''
    set_weights(g, 1.0)


def set_weights(g, weight):
    '''Set edge weights for NetworkX graph to specified weight & return.'''
    for src, dst in g.edges():
        g[src][dst]['weight'] = weight
    return g


def nx_graph_from_tuples(undir_tuples, dir_tuples = None):
    g = nx.Graph()
    for a, b, w in undir_tuples:
        g.add_edge(a, b, weight = w)
    if dir_tuples:
        g = nx.DiGraph(g)
        for a, b, w in dir_tuples:
            g.add_edge(a, b, weight = w)
    return g

def nx_indexed_edges_digraph_from_tuples(dir_tuples):
    g = nx.DiGraph()
    for a, b, w, i in dir_tuples:
        g.add_edge(a, b, weight = w, index = i)
    return g

def vertex_disjoint(paths):
    '''Check if provided paths are vertex-disjoint.

    @param paths: list of path lists
    '''
    vertices = set([])
    for path in paths:
        for n in path:
            if n in vertices:
                return False
            vertices.add(n)
    return True


def edge_disjoint(paths):
    '''Check if provided paths are edge-disjoint.

    @param paths: list of path lists
    '''
    edges = set([])
    # Ensure edge disjointness
    for path in paths:
        for i, n in enumerate(path):
            if i != len(path) - 1:
                e = (n, path[i + 1])
                if e in edges:
                    return False
                edges.add(e)
                e_rev = (path[i + 1], n)
                if e_rev in edges:
                    return False
                edges.add(e_rev)
    return True


def pathlen(g, path):
    '''Return sum of path weights.

    @param g: NetworkX Graph
    @param path: list of nodes
    @return length: sum of path weights
    '''
    pathlen = 0
    for i, n in enumerate(path):
        if i != len(path) - 1:
            pathlen += g[n][path[i+1]]['weight']
    return pathlen

def pathlen_MultiDiGraph(g, path,keys):
    '''Return sum of path weights in a Multi edge Directed Graph.

    @param g: NetworkX Multi edge Directed Graph (MultiDiGraph)
    @param path: list of nodes
    @return length: sum of path weights
    '''
    pathlen = 0
    for i, n in enumerate(path):
        if i != len(path) - 1:
            pathlen += g[n][path[i+1]][keys[i]]['weight']
    return pathlen

def edges_on_path(l):
    '''Return list of edges on a path list.'''
    return [(l[i], l[i + 1]) for i in range(len(l) - 1)]


def labels_on_path(g,path,path_key):
    '''Return list of labels of each edge on a path list
    @param g: NetworkX Graph
    @param path: list of nodes
    @param path_keys: list of edges' keys to distinguish multiedges in path
    @return label_list: list of labes of edges on path
    '''
    label_list = []
    for i, node in enumerate(path):
        if i != len(path) - 1:
            #label_list.append(g.get_edge_data(node,path[i+1],key=path_key[i],'label')
            label_list.append(g[node][path[i+1]][path_key[i]]['label'])
    return label_list

def indexes_on_path(g,path):
    '''Return a list of indexes/labels corresponding to each edge on a path list
    @param g: NetworkX Graph
    @param path: list of nodes
    @return label_list: list of indexes/labes of edges on path
    '''
    label_list = []
    for i, node in enumerate(path):
        if i != len(path) - 1:
            label_list.append(g[node][path[i+1]]['label'])
    return label_list


def all_simple_paths_MultiDiGraph_with_keys(G, source, target, cutoff=None):
    '''Generate all simple paths in the MultiDiGraph G from source to target together 
    with the edges key of the paths.
    
    MultiDiGraph: Multi-edges directed Graph.

    A simple path is a path with no repeated nodes.
    This is a modification of the function  _all_simple_paths_multigraph from 
    networkX Python language software package, that also returns the keys of
    the edges in order to distinguish multiedges between same pair of nodes.
    
    Parameters
    ----------
    @param G : NetworkX graph

    @param source : node
       Starting node for path

    @param target : node
       Ending node for path

    @param cutoff : integer, optional
        Depth to stop the search. Only paths of length <= cutoff are returned.

    Returns
    -------
    @return simple_path, simple_path_key : generator
    
    A generator that produces a tuple of lists of simple_path,simple_path_keys
    simple_paths: lists of simple paths.  If there are no paths
       between the source and target within the given cutoff return empty
    simple_paths_key: list of dges keys associated with the simple paths.
    
    Notes
    -----
    This algorithm uses a modified depth-first search to generate the
    paths [1]_.  A single path can be found in `O(V+E)` time but the
    number of simple paths in a graph can be very large, e.g. `O(n!)` in
    the complete graph of order n.

    References
    ----------
    .. [1] R. Sedgewick, "Algorithms in C, Part 5: Graph Algorithms",
       Addison Wesley Professional, 3rd ed., 2001.
       
    .. http://networkx.github.io
    '''
    if source not in G:
        raise nx.NetworkXError('source node %s not in graph'%source)
    if target not in G:
        raise nx.NetworkXError('target node %s not in graph'%target)
    if cutoff is None:
        cutoff = len(G)
    
    if cutoff < 1:
        return
    visited = [source]
    visited_edge_key=[]  #Like visited nodes, but with the keys of visited edges
    
    stack = [(v for u,v in G.out_edges(source))]  #this is a list containing one generator expresion
    stack_edge_key = [(z for x,y,z in G.out_edges(source, keys=True))]
    
    while stack:
        children = stack[-1]            #get the last generator expresion in stack
        child = next(children, None)    #get the next v of the generator children
        
        children_edge_keys = stack_edge_key[-1]
        child_edge_key = next(children_edge_keys,None)
        #print ('Edge append to node %s '%child)
        #print ('Edge append with key= %s '%child_edge_key)
        
        if child is None or child_edge_key is None:       # there is no child
            #print 'Pop'
            stack.pop()
            stack_edge_key.pop()
            visited.pop()
            if visited_edge_key:
                visited_edge_key.pop()
            
        elif len(visited) < cutoff:
            if child == target:
                #print 'New Output'
                #print ("Output: %s , %s" % (visited + [child], visited_edge_key + [child_edge_key]))
                yield visited + [child], visited_edge_key + [child_edge_key]
                
            elif child not in visited:
                visited.append(child)
                visited_edge_key.append(child_edge_key)
                stack.append((v for u,v in G.out_edges(child)))
                stack_edge_key.append((z for x,y,z in G.out_edges(child, keys=True)))
                #print ('Visited %s '%visited)
                #print ('Visited edges keys %s '%visited_edge_key)
                
            elif visited_edge_key[visited.index(child)] != child_edge_key:
                print ('New multiedge in a already visited node!')
                visited_edge_key[visited.index(child)] = child_edge_key
                stack.append((v for u,v in G.out_edges(child)))
                stack_edge_key.append((z for x,y,z in G.out_edges(child, keys=True)))
                print ('Visited %s '%visited)
                print ('Visited edges keys %s '%visited_edge_key)
                
        else: #len(visited) == cutoff:
            count = ([child]+list(children)).count(target) # Counts how many target are in child U children
            for i in range(count):
                yield visited + [target] , visited_edge_key + [child_edge_key]
                #print'Output with len(visited)=cutoff'
                
            stack.pop()
            visited.pop()
            stack_edge_key.pop()
            visited_edge_key.pop()
            
    #return all_simple_paths, all_simple_paths_key

def interlacing_edges(list1, list2):
    '''Return edges in common between two paths.

    Input paths are considered interlacing, even if they go in the opposite
    direction across the same link.  In that case, a single edge will be
    return in whatever order NetworkX prefers for an undirected edge.
    '''
    l1 = edges_on_path(list1)
    l1.extend(edges_on_path([i for i in reversed(list1)]))
    l2 = edges_on_path(list2)
    l2.extend(edges_on_path([i for i in reversed(list2)]))
    combined = [e for e in l1 if e in l2]
    return nx.Graph(combined).edges()


def flip_and_negate_path(g, path):
    '''Return new directed graph with the given path flipped & negated.

    @param g: NetworkX Graph (undirected)
    @param path: list of nodes in path
    @return g2: NetworkX DiGraph, modified
    '''
    g2 = nx.DiGraph(g)

    for i, n in enumerate(path):
        if i != len(path) - 1:
            n_next = path[i + 1]
            # Remove forward edge, leaving only reverse edge.
            g2.remove_edge(n, n_next)
            # Flip edge weight to negative of the original edge..
            g2[n_next][n]['weight'] *= -1

    return g2


def remove_edge_bidir(g, src, dst):
    '''Remove edge plus one in opposite direction.

    @param g: NetworkX DiGraph
    @param src: source node
    @param dst: destination node
    '''
    g.remove_edge(src, dst)
    g.remove_edge(dst, src)


def add_edge_bidir(g, src, dst, weight = None):
    '''Add edge plus one in opposite direction.

    @param g: NetworkX DiGraph
    @param src: source node
    @param dst: destination node
    @param weight: optional weight to set for both
    '''
    g.add_edge(src, dst)
    g.add_edge(dst, src)
    if weight:
        g[src][dst]['weight'] = weight
        g[dst][src]['weight'] = weight


def increase_and_negate_paths(g, path_set, inf2):
    '''Return new directed graph with the given path flipped & negated.

    @param g: NetworkX DiGraph (directed)
    @param path_set: set of path (list of nodes in the path)
    @param inf2: big positive number
    @return g2: NetworkX DiGraph, modified
    '''
    g2 = nx.DiGraph(g)
    #path_set = list(path_set)
    for m in range (0,len(path_set)):
        for i, n in enumerate(path_set[m]):
            if i != len(path_set[m]) - 1:
                n_next = path_set[m][i + 1]
                # Increase by INF2 the weight of the forward edge.
                g2[n][n_next]['weight'] += inf2 
                # Negate the weight of the backward edge.
                g2[n_next][n]['weight'] *= -1
    return g2